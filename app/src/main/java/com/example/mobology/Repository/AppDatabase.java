package com.example.mobology.Repository;

import android.content.Context;
import android.util.Log;

import com.example.mobology.models.DeviceEntry;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {DeviceEntry.class} , version = 1 , exportSchema = false )
public abstract class AppDatabase extends RoomDatabase {

    private static final String TAG = "AppDatabase";
    private static final Object LOCK = new Object() ;
    public static final String DATABASE_NAME = "Devices_db" ;
    private static AppDatabase sInstance ;

    public static AppDatabase getInstance(Context context){

        if( sInstance == null ){
            Log.d(TAG, "getInstance: creating a new database ");
            sInstance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class,
                    DATABASE_NAME)
                    .build() ;
        } else {
            Log.d(TAG, "getInstance: returning an old instance ");
        }
        return sInstance ;
    }
    public abstract DeviceDao deviceDao() ;


}
