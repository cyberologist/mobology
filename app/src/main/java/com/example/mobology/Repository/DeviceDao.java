package com.example.mobology.Repository;

import com.example.mobology.models.DeviceEntry;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


@Dao
public interface DeviceDao {

//    @Query("Select * from Devices ORDER BY id")
//    LiveData<List<DeviceEntry>> loadAllDevices();
//
    @Delete
    void deleteDevice(DeviceEntry deviceEntry) ;
//
    @Insert
    void insertDevice(DeviceEntry deviceEntry) ;

    @Query("Select * from Devices WHERE id = :DeviceId")
    DeviceEntry getDeviceById(Integer DeviceId) ;
    @Query("Select * from Devices ")
    LiveData<List<DeviceEntry>>queryAllDevices() ;
}
