package com.example.mobology.Repository;

import android.content.Context;
import android.util.Log;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.utils.AppExecutors;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import rx.Observable;

public class LocalRepository {

    private static final String TAG = "LocalRepository";

    private AppDatabase appDatabase ;
    private Repository.AsyncQueryGetter queryGetter ;

    public LocalRepository(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    public void requestAllDevices(final Repository.AsyncQueryGetter queryGetter){
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final LiveData<List<DeviceEntry>> listLiveData = appDatabase.deviceDao().queryAllDevices() ;
                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        queryGetter.returnedQuery(listLiveData);
                    }
                });
            }
        });
    }
    public void removeFromDatabase(final Context context, final DeviceEntry deviceEntry) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(context).deviceDao().deleteDevice(deviceEntry);
                Log.d(TAG, "run: deleted");
            }
        });
    }
    public void removeFromDatabase(final Repository.QueryOperationsEventsListener listener, final Context context, final DeviceEntry deviceEntry) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(context).deviceDao().deleteDevice(deviceEntry);

                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        listener.notifyDone();
                    }
                });

            }
        });
    }

    public Observable<List<DeviceEntry>> getDeviceFromMemory(Integer deviceId) {
        DeviceEntry deviceEntry = appDatabase.deviceDao().getDeviceById(deviceId);
        if(deviceEntry == null){
            return Observable.empty();
        }
        deviceEntry.setFavorite(true);
        List<DeviceEntry> ls = new ArrayList<>() ;
        Log.d(TAG, "getDeviceFromMemory: called with" + deviceEntry.getName());
        ls.add(deviceEntry) ;
        return Observable.just(ls) ;
    }
    public void addToDatabase(final Repository.QueryOperationsEventsListener listener, final Context context, final  DeviceEntry deviceEntry) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(context).deviceDao().insertDevice(deviceEntry);

                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        listener.notifyDone();
                    }
                });
            }
        });
    }
}
