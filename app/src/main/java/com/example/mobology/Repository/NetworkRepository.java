package com.example.mobology.Repository;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;

import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

public class NetworkRepository {

    // TODO Make it just network repository
    private RetrofitClient retrofitClient ;
    private LocalRepository localRepository;

    public NetworkRepository(RetrofitClient retrofitClient, LocalRepository localRepository) {
        this.retrofitClient = retrofitClient;
        this.localRepository = localRepository;
    }

    public Observable<List<DeviceEntry>> getDeviceFromNetwork(Integer deviceId) {
        return retrofitClient.getDeviceById(deviceId) ;
    }

    public Observable<List<DeviceEntry>> getDevice(Integer deviceId) {
        return localRepository.getDeviceFromMemory(deviceId).switchIfEmpty(getDeviceFromNetwork(deviceId)) ;
    }

    public Observable<DeviceEntryWithImage> getDeviceWithImage(Integer deviceId){

        return getDevice(deviceId).concatMap(new Func1<List<DeviceEntry>, Observable<DeviceEntryWithImage>>() {
            @Override
            public Observable<DeviceEntryWithImage> call(List<DeviceEntry> deviceEntries) {
                if(deviceEntries != null && deviceEntries.size() != 0){
                    return Observable.zip(Observable.just(deviceEntries.get(0)),
                            getImageFromUrl(deviceEntries.get(0).getImageUrl()), new Func2<DeviceEntry, Bitmap, DeviceEntryWithImage>() {
                                @Override
                                public DeviceEntryWithImage call(DeviceEntry deviceEntry, Bitmap bitmap) {
                                    return new DeviceEntryWithImage(deviceEntry , bitmap) ;
                                }
                            });
                } else {
                    return Observable.empty() ;
                }
            }
        });
    }

    public Observable<Bitmap> getImageFromUrl(String imageUrl) {
        return retrofitClient.downloadImageFromUrl(imageUrl).concatMap(new Func1<ResponseBody, Observable<Bitmap>>() {
            @Override
            public Observable<Bitmap> call(ResponseBody responseBody) {
                return Observable.just(BitmapFactory.decodeStream(responseBody.byteStream()));
            }
        });
    }

    public Observable<List<DeviceEntry>> getSearchResults(String searchText) {
        return retrofitClient.getSearchResult(searchText) ;
    }

    public Observable<DeviceEntry> getRandomItemsFromNetwork(){
        return retrofitClient.get10Random().concatMap(new Func1<List<DeviceEntry>, Observable<DeviceEntry>>() {
            @Override
            public Observable<DeviceEntry> call(List<DeviceEntry> deviceEntries) {
                if(deviceEntries == null || deviceEntries.size() == 0 )
                    return Observable.empty() ;
                return Observable.from(deviceEntries);
            }
        });
    }

    public Observable<DeviceEntryWithImage> getRandomItemsFromNetworkWithImages() {
        return getRandomItemsFromNetwork().concatMap(new Func1<DeviceEntry, Observable<DeviceEntryWithImage>>() {
            @Override
            public Observable<DeviceEntryWithImage> call(DeviceEntry deviceEntry) {
                return Observable.zip(Observable.just(deviceEntry), getImageFromUrl(deviceEntry.getImageUrl()), new Func2<DeviceEntry, Bitmap, DeviceEntryWithImage>() {
                    @Override
                    public DeviceEntryWithImage call(DeviceEntry deviceEntry, Bitmap bitmap) {
                        return new DeviceEntryWithImage(deviceEntry , bitmap) ;
                    }
                });
            }
        }) ;

    }
    Observable<DeviceEntry> getDevicesByName(String query){
        return retrofitClient.getSearchResult(query).concatMap(new Func1<List<DeviceEntry>, Observable<DeviceEntry>>() {
            @Override
            public Observable<DeviceEntry> call(List<DeviceEntry> deviceEntries) {
                if(deviceEntries != null && deviceEntries.size() != 0 ){
                    return Observable.from(deviceEntries) ;
                } else {
                    return Observable.empty() ;
                }
            }
        }) ;
    }

    Observable<DeviceEntryWithImage> getDevicesByNameWithImages(String query){
        return getDevicesByName(query).concatMap(new Func1<DeviceEntry, Observable<DeviceEntryWithImage>>() {
            @Override
            public Observable<DeviceEntryWithImage> call(DeviceEntry deviceEntry) {
                return Observable.zip(Observable.just(deviceEntry),
                        getImageFromUrl(deviceEntry.getImageUrl()),
                        new Func2<DeviceEntry, Bitmap, DeviceEntryWithImage>() {
                            @Override
                            public DeviceEntryWithImage call(DeviceEntry deviceEntry, Bitmap bitmap) {
                                return new DeviceEntryWithImage(deviceEntry , bitmap) ;
                            }
                        });
            }
        }) ;
    }

}
