package com.example.mobology.Repository;

import android.content.Context;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;

import java.util.List;

import androidx.lifecycle.LiveData;
import rx.Observable;

public class Repository {

    private LocalRepository localRepository ;
    private NetworkRepository networkRepository ;

    public interface AsyncQueryGetter{
        void returnedQuery(LiveData<List<DeviceEntry>> deviceEntries) ;
    }
    public interface QueryOperationsEventsListener {
        void notifyDone() ;
    }
    public Repository(NetworkRepository networkRepository , LocalRepository localRepository) {
        this.localRepository = localRepository;
        this.networkRepository = networkRepository;
    }


    public Observable<DeviceEntryWithImage> getDevicesByNameWithImages(String category) {
        return networkRepository.getDevicesByNameWithImages(category);
    }


    public Observable<List<DeviceEntry>> getDevice(Integer deviceId){
        return localRepository.getDeviceFromMemory(deviceId)
                .switchIfEmpty(networkRepository.getDeviceFromNetwork(deviceId)) ;
    }


    public Observable<DeviceEntryWithImage> getDeviceWithImage(Integer deviceId){
        return networkRepository.getDeviceWithImage(deviceId);
    }

    public void removeFromDatabase(QueryOperationsEventsListener listener , final Context context, final DeviceEntry deviceEntry){
        localRepository.removeFromDatabase(listener , context , deviceEntry );
    }


    public void removeFromDatabase(final Context context, final DeviceEntry deviceEntry){
        localRepository.removeFromDatabase(context , deviceEntry);
    }


    public void addToDatabase(QueryOperationsEventsListener listener, Context context, DeviceEntry deviceEntry){
        localRepository.addToDatabase(listener , context , deviceEntry);
    }


    public void requestAllDevices(AsyncQueryGetter queryGetter){
        localRepository.requestAllDevices(queryGetter);
    }


    public Observable<DeviceEntry> getRandomItemsFromNetwork(){
        return networkRepository.getRandomItemsFromNetwork();
    }


    public Observable<DeviceEntryWithImage> getRandomItemsFromNetworkWithImages(){
        return networkRepository.getRandomItemsFromNetworkWithImages();
    }


    public Observable<List<DeviceEntry>> getSearchResults(String searchText){
        return networkRepository.getSearchResults(searchText) ;
    }


}
