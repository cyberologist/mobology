package com.example.mobology.Repository;

import android.content.Context;

import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RepositoryModule {


    static  String BaseUrl = "https://mopology.herokuapp.com/" ;
//    static  String BaseUrl = "http://192.168.1.7:3000/" ;
//    static  String BaseUrl = "http://10.0.2.2:8000/" ;


    @Singleton
    @Provides
    public Repository provideRepository(NetworkRepository networkRepository , LocalRepository localRepository){
        return new Repository(networkRepository , localRepository) ;
    }
    @Singleton
    @Provides
    public NetworkRepository provideNewtworkRepository(RetrofitClient retrofitClient , LocalRepository localRepository ){
        return new NetworkRepository(retrofitClient , localRepository ) ;
    }

    @Singleton
    @Provides
    public LocalRepository providLocalRepository(AppDatabase appDatabase){
        return new LocalRepository(appDatabase) ;
    }

    @Singleton
    @Provides
    public AppDatabase provideAppDatabase(Context context){
        return Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class,
                AppDatabase.DATABASE_NAME)
                // only use that when testing TEMPORARY
                .allowMainThreadQueries()
                .build() ;
    }

    @Singleton
    @Provides
    public DeviceDao provideDeviceDao(AppDatabase appDatabase){
        return appDatabase.deviceDao() ;
    }

    @Provides
    public OkHttpClient provideClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build() ;

    }

    @Provides
    public Retrofit provideRetrofit(String baseURL, OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    public RetrofitClient providRetrofit() {
        return provideRetrofit(BaseUrl, provideClient()).create(RetrofitClient.class);
    }

}
