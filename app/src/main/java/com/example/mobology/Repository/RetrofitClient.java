package com.example.mobology.Repository;

import com.example.mobology.models.DeviceEntry;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import rx.Observable;

public interface RetrofitClient {

    @GET("api/phones/search/{name}")
    Observable<List<DeviceEntry>>  getSearchResult(@Path("name") String Query) ;

    @GET("images/{name}")
    Observable<ResponseBody> downloadImageFromUrl(@Path("name") String imageUrl);

    @GET("api/phones/show/{id}")
    Observable<List<DeviceEntry>> getDeviceById(@Path("id") Integer id) ;

    @GET("api/phones")
    Observable<List<DeviceEntry>> get10Random() ;

}
