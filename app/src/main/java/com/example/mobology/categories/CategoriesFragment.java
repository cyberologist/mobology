package com.example.mobology.categories;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mobology.BuildConfig;
import com.example.mobology.models.DeviceEntryWithImage;
import com.example.mobology.R;
import com.example.mobology.root.App;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import static com.example.mobology.utils.consts.APPLE_FRAGMENT;
import static com.example.mobology.utils.consts.FRAGMENT_TYPE_INDICATOR;
import static com.example.mobology.utils.consts.HAWAWI_FRAGMENT;
import static com.example.mobology.utils.consts.SAMSUNG_FRAGMENT;
import static com.example.mobology.utils.consts.SONY_FRAGMENT;
import static com.example.mobology.utils.consts.TRENDING_FRAGMENT;

public class CategoriesFragment extends Fragment implements CategoriesMVP.View {

    private static final String TAG = "CategoriesFragment";

    @Inject
    CategoriesMVP.Presenter presenter ;

    private StaggeredRecyclerViewAdapter mAdepter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar ;
    private ImageView loadingResult ;
    private String category;
    public static final int COLUMN_NUM = 2 ;
    private int fragmentId ;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setRetainInstance(true);

        ((App) getActivity().getApplication()).getComponent().inject(this);

        View v = inflater.inflate(R.layout.fragment_categories, container, false);

        if (getArguments() != null) {
            fragmentId = getArguments().getInt(FRAGMENT_TYPE_INDICATOR, -1);
            Log.d(TAG, "onCreateView: fragment Id : " + fragmentId);
        }


        progressBar = v.findViewById(R.id.categories_progress) ;
        loadingResult = v.findViewById(R.id.categories_loading_result_image) ;
        Toolbar mTopToolbar;
        mTopToolbar = v.findViewById(R.id.my_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mTopToolbar);


        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), drawer, mTopToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mAdepter = new StaggeredRecyclerViewAdapter(getContext());
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(COLUMN_NUM
                , LinearLayoutManager.VERTICAL);

        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);

        recyclerView = v.findViewById(R.id.categories_recycler_view);

        // for handling a bug (the item takes different spans than the original when scrolling )
        recyclerView.setItemAnimator(null);

        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(mAdepter);

        switch (fragmentId) {

            case TRENDING_FRAGMENT:
                category = "trending";
                break;
            case SAMSUNG_FRAGMENT:
                category = ("samsung");
                break;
            case SONY_FRAGMENT:
                category = ("sony");
                break;
            case HAWAWI_FRAGMENT:
                category = ("hawawi");
                break;
            case APPLE_FRAGMENT:
                category = ("apple");
                break;
            default:
                Log.wtf(TAG, "onCreateView: ID DOESN'T EXIST");
        }

        if (BuildConfig.DEBUG) {
            category = "a";
        }

        presenter.setView(this);
        presenter.requestCategoryItems(category);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.rxUnsubscribe();
    }

    @Override
    public void notifyError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.GONE);
        loadingResult.setVisibility(View.VISIBLE);
        loadingResult.setImageResource(R.drawable.error_loading_small);
    }

    @Override
    public void notifyCompleted() {
        progressBar.setVisibility(View.GONE);
        loadingResult.setVisibility(View.VISIBLE);
        loadingResult.setImageResource(R.drawable.check_circle);
        Handler h = new Handler(Looper.getMainLooper()) ;
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadingResult.setVisibility(View.GONE);
            }
        } , 1000) ;
    }

    @Override
    public void addCategoryItem(final DeviceEntryWithImage deviceEntryWithImage) {
        mAdepter.addToContent(deviceEntryWithImage);
    }
}
