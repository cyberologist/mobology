package com.example.mobology.categories;


import com.example.mobology.models.DeviceEntryWithImage;


import rx.Observable;

public interface CategoriesMVP {
    interface Presenter {
        void rxUnsubscribe();
        void requestCategoryItems(String searchText) ;
        void setView(CategoriesMVP.View view) ;
    }
    interface Model {
        Observable<DeviceEntryWithImage> getResult(String category) ;
    }
    interface View {
        void notifyError(String message) ;
        void notifyCompleted() ;
        void addCategoryItem(DeviceEntryWithImage deviceEntryWithImage) ;
    }
}
