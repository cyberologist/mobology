package com.example.mobology.categories;

import com.example.mobology.Repository.Repository;
import com.example.mobology.models.DeviceEntryWithImage;

import rx.Observable;

public class CategoriesModel implements CategoriesMVP.Model {

    Repository repository ;

    public CategoriesModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<DeviceEntryWithImage> getResult(String category) {
        return repository.getDevicesByNameWithImages(category);
    }
}
