package com.example.mobology.categories;

import com.example.mobology.Repository.Repository;
import com.example.mobology.Repository.RetrofitClient;

import dagger.Module;
import dagger.Provides;

@Module
public class CategoriesModule {

    @Provides CategoriesMVP.Presenter provideCategoriesPresenter(CategoriesMVP.Model model){
        return new CategoriesPresenter(model);
    }
    @Provides CategoriesMVP.Model provideCategoriesModel(Repository repository){
        return new CategoriesModel(repository);
    }

}
