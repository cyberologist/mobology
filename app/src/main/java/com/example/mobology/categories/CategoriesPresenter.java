package com.example.mobology.categories;

import android.util.Log;

import com.example.mobology.models.DeviceEntryWithImage;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CategoriesPresenter implements CategoriesMVP.Presenter {


    private static final String TAG = "CategoriesPresenter";

    private CategoriesMVP.Model model ;
    private CategoriesMVP.View view ;
    private Subscription subscription = null ;

    public CategoriesPresenter(CategoriesMVP.Model model) {
        this.model = model;
    }

    @Override
    public void rxUnsubscribe() {
        if(subscription != null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void requestCategoryItems(String searchText) {
        subscription = model.getResult(searchText)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<DeviceEntryWithImage>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: completed :) ");
                        if(view != null){
                            view.notifyCompleted();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: error loading from categories ):");
                        e.printStackTrace();
                        if(view != null){
                            view.notifyError("error loading , sorry ):");
                        }
                    }

                    @Override
                    public void onNext(DeviceEntryWithImage deviceEntryWithImage) {
                        if(view != null){
                            view.addCategoryItem(deviceEntryWithImage);
                        }
                    }
                });
    }

    @Override
    public void setView(CategoriesMVP.View view) {
        this.view = view ;
    }
}
