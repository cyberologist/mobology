package com.example.mobology.categories;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.mobology.R;
import com.example.mobology.models.DeviceEntryWithImage;
import com.example.mobology.details.DeviceDetails;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import static com.example.mobology.utils.consts.INTENT_DEVICE_ID;
import static com.example.mobology.utils.consts.INTENT_DEVICE_NAME;

public class StaggeredRecyclerViewAdapter extends RecyclerView.Adapter<StaggeredRecyclerViewAdapter.GridLayoutViewHolder>{

    private static final String TAG = "StaggeredRecyclerViewAd";

    private Context context ;
    private List<DeviceEntryWithImage> content ;



    public StaggeredRecyclerViewAdapter(Context context) {
        this.context = context;
        this.content = new ArrayList<>();
    }

    @NonNull
    @Override
    public GridLayoutViewHolder onCreateViewHolder(final @NonNull ViewGroup parent, final int viewType) {
        final View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.categories_grid_layout , parent , false ) ;

        return new GridLayoutViewHolder(v) ;
    }

    @Override
    public void onBindViewHolder(@NonNull GridLayoutViewHolder holder, final int position) {


        final DeviceEntryWithImage item = content.get(position);

            holder.deviceItem.setClickable(true);
            holder.deviceItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context , DeviceDetails.class) ;
                    intent.putExtra(INTENT_DEVICE_ID , item.deviceEntry.getId()) ;
                    intent.putExtra(INTENT_DEVICE_NAME, item.deviceEntry.getName()) ;
                    (context).startActivity(intent);
                }
            });
            holder.mainName.setText(item.deviceEntry.getName());
            holder.subName.setText(item.deviceEntry.getBrand());

        ((StaggeredGridLayoutManager.LayoutParams)holder.itemView.getLayoutParams()).setFullSpan((position % 3 == 0 ));

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_launcher_background);
        Glide.with(context)
                .load(item.image)
                .apply(requestOptions)
                .into(holder.deviceImage);

    }

    @Override
    public int getItemCount() {
        return content.size() ;
    }

    public class GridLayoutViewHolder extends RecyclerView.ViewHolder {
        CardView deviceItem ;
        TextView mainName ;
        TextView subName ;
        ImageView deviceImage ;

        public GridLayoutViewHolder(@NonNull View itemView) {
            super(itemView);
            deviceItem = itemView.findViewById(R.id.categories_grid_card_view) ;
            deviceImage = itemView.findViewById(R.id.categories_grid_layout_image) ;
            mainName = itemView.findViewById(R.id.categories_grid_layout_name) ;
            subName = itemView.findViewById(R.id.categories_grid_layout_brand) ;
        }
    }

    public void addToContent(DeviceEntryWithImage deviceEntryWithImage){
        content.add(deviceEntryWithImage);
        if(content.size() == 0){
            notifyItemInserted(0);
        } else {
            notifyItemInserted(content.size() -1);
        }
    }
}
