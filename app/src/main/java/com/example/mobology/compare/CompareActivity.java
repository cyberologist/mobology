package com.example.mobology.compare;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.example.mobology.R;
import com.example.mobology.root.App;

import org.javatuples.Pair;
import org.javatuples.Triplet;

import java.util.List;

import javax.inject.Inject;

import static com.example.mobology.compare.CompareFragment.FIRST_SELECTED_DEVICE_ID;
import static com.example.mobology.compare.CompareFragment.FIRST_SELECTED_DEVICE_NAME;
import static com.example.mobology.compare.CompareFragment.SECOND_SELECTED_DEVICE_ID;
import static com.example.mobology.compare.CompareFragment.SECOND_SELECTED_DEVICE_NAME;


public class CompareActivity extends AppCompatActivity implements CompareMVP.View{

    private static final String TAG = "CompareActivity";

    ImageView compareDeviceImageFr ;
    ImageView compareDeviceImageSe ;
    Toolbar compareToolBar ;
    ProgressBar progressBar ;

    
    @Inject
    CompareMVP.Presenter presenter ;

    int firstDeviceId = -1  ;
    int secondDeviceId = -1 ;

    String secondDeviceName ;
    String firstDeviceName ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((App) getApplication()).getComponent().inject(this);

        setContentView(R.layout.activity_compare);

        compareDeviceImageFr = findViewById(R.id.compare_device_image_fr) ;
        compareDeviceImageSe = findViewById(R.id.compare_device_image_se) ;

        progressBar = findViewById(R.id.compare_loading_bar) ;

//        compareDeviceImageFr.setImageResource(R.drawable.device_pic_plpaceholder);
//        compareDeviceImageFr.setImageResource(R.drawable.ic_launcher_foreground);

        compareDeviceImageSe.setVisibility(View.GONE);
        compareDeviceImageFr.setVisibility(View.GONE);

        Intent myStarter = getIntent() ;
        if(myStarter != null ){
            firstDeviceId = myStarter.getIntExtra(FIRST_SELECTED_DEVICE_ID , -1 ) ;
            secondDeviceId = myStarter.getIntExtra(SECOND_SELECTED_DEVICE_ID , -1 ) ;
            firstDeviceName = myStarter.getStringExtra(FIRST_SELECTED_DEVICE_NAME) ;
            secondDeviceName = myStarter.getStringExtra(SECOND_SELECTED_DEVICE_NAME) ;
            Log.d(TAG, "onCreate: " + firstDeviceId );
            Log.d(TAG, "onCreate: " + secondDeviceId );
            Log.d(TAG, "onCreate: " + firstDeviceName);
            Log.d(TAG, "onCreate: " + secondDeviceName);

        }
        //chang to loading..
        compareToolBar = findViewById(R.id.compare_appbar_title) ;
        compareToolBar.setTitle("1                  |  2 ");
        setSupportActionBar(compareToolBar);
        if(getSupportActionBar() != null ){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if( firstDeviceId == -1 || secondDeviceId == -1){
            Toast.makeText(this , "error ): , please try again later" , Toast.LENGTH_LONG).show();
            Log.e(TAG, "onCreate: error loading the ids from starter intent");
        }

        presenter.setView(this);
        progressBar.setVisibility(View.VISIBLE);
        presenter.requestDetails(firstDeviceId , secondDeviceId);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.rxUnsubscribe();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this , message , Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateData(List<Triplet<String, String, String>> compareDetails) {

        compareDeviceImageSe.setVisibility(View.VISIBLE);

        CompareAdapter mAdapter = new CompareAdapter(this , compareDetails ,firstDeviceName , secondDeviceName ) ;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView compareDevicesAttrs ;
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());

        compareDevicesAttrs = findViewById(R.id.compare_devices_attrs) ;
        compareDevicesAttrs.setAdapter(mAdapter);
        compareDevicesAttrs.setLayoutManager(layoutManager) ;
        compareDevicesAttrs.addItemDecoration(dividerItemDecoration);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void updateDataWithImages(Pair<Pair<Bitmap, Bitmap>, List<Triplet<String, String, String>>> compareDetails) {

        compareDeviceImageSe.setVisibility(View.VISIBLE);
        compareDeviceImageFr.setVisibility(View.VISIBLE);
        compareDeviceImageFr.setImageBitmap(compareDetails.getValue0().getValue0());
        compareDeviceImageSe.setImageBitmap(compareDetails.getValue0().getValue1());

        CompareAdapter mAdapter = new CompareAdapter(this , compareDetails.getValue1() ,firstDeviceName , secondDeviceName ) ;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView compareDevicesAttrs ;
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());

        compareDevicesAttrs = findViewById(R.id.compare_devices_attrs) ;
        compareDevicesAttrs.setAdapter(mAdapter);
        compareDevicesAttrs.setLayoutManager(layoutManager) ;
        compareDevicesAttrs.addItemDecoration(dividerItemDecoration);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "onBackPressed: pressed");
        finish();
    }




}
