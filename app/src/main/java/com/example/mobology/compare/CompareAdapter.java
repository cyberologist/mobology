package com.example.mobology.compare;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mobology.R;

import org.javatuples.Triplet;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.annotations.NonNull;

public class CompareAdapter extends RecyclerView.Adapter<CompareAdapter.CompareTextViewHolder> {

    private static final String TAG = "CompareAdapter";

    private List<Triplet<String, String, String>> content;
    private Context context;

    CompareAdapter(Context context, List<Triplet<String, String, String>> content , String fr , String se ) {
        this.context = context;
        content.add(0 , new Triplet<>("name" , fr , se ));
        this.content = content;
    }

    @Override
    public CompareTextViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View v = ((Activity) context).getLayoutInflater().inflate(R.layout.compare_item, parent, false);
        return new CompareTextViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CompareTextViewHolder holder, int position) {

        if (content == null) {
            holder.attrName.setText("Sorry a Network error occurred ):");
            holder.attrName.setTextSize(36f);
            holder.firstDeviceAttr.setText("");
            holder.secondDeviceAttr.setText("");
        } else {
            holder.attrName.setTextSize(18f);
            holder.attrName.setText(content.get(position).getValue0());
            holder.firstDeviceAttr.setText(content.get(position).getValue1());
            holder.secondDeviceAttr.setText(content.get(position).getValue2());
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: " + (content == null || content.size() == 0 ? "1" : "" + content.size()));
        return content == null || content.size() == 0 ? 1 : content.size();
    }


    class CompareTextViewHolder extends RecyclerView.ViewHolder {
        TextView attrName;
        TextView firstDeviceAttr;
        TextView secondDeviceAttr;

        CompareTextViewHolder(@NonNull View itemView) {
            super(itemView);
            attrName = itemView.findViewById(R.id.compare_item_attr_name);
            firstDeviceAttr = itemView.findViewById(R.id.compare_item_attr_fr);
            secondDeviceAttr = itemView.findViewById(R.id.compare_item_attr_se);
        }

    }
}

