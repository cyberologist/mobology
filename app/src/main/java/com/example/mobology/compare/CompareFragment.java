package com.example.mobology.compare;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.mobology.R;
import com.example.mobology.search.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

public class CompareFragment extends Fragment implements View.OnClickListener{

    private static final String TAG = "CompareFragment";

    private Toolbar mTopToolbar;

    public static final int FIRST_DEVICE_SELECT_REQUEST_CODE = 747;
    public static final int SECOND_DEVICE_SELECT_REQUEST_CODE = 757;
    public static final int DEVICE_SELECT_REQUEST_CODE = 767;
    public static final int DEVICE_SELECT_RESULT_CODE = 777;

    public static final String SEARCHED_DEVICE_ID = "sdi" ;
    public static final String SEARCHED_DEVICE_NAME = "sdn" ;

    public static final String FIRST_SELECTED_DEVICE_ID = "fsdi" ;
    public static final String SECOND_SELECTED_DEVICE_ID = "ssdi" ;
    public static final String FIRST_SELECTED_DEVICE_NAME = "fsdn" ;
    public static final String SECOND_SELECTED_DEVICE_NAME = "ssdn" ;


    private TextView firstPrompt ;
    private TextView secondPrompt ;
    private ImageView firstSelected ;
    private ImageView secondSelected ;
    private ImageView bothSelected ;
    private Button selectFirst ;
    private Button selectSecond ;
    private Button actionCompare ;

    private Integer firstDeviceId = null  ;
    private Integer secondDeviceId = null ;
    private String secondDeviceName ;
    private String firstDeviceName ;

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        if( savedInstanceState != null){
            Log.wtf(TAG, "onCreate: god");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        setRetainInstance(true);


        Log.d(TAG, "onCreateView: ");
        View v = inflater.inflate(R.layout.fragment_compare, container , false) ;


        mTopToolbar = v.findViewById(R.id.my_toolbar);

        ((AppCompatActivity) getActivity()).setSupportActionBar(mTopToolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, mTopToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        firstSelected = v.findViewById(R.id.compare_isselected_first) ;
        secondSelected = v.findViewById(R.id.compare_isselected_second) ;
        firstPrompt = v.findViewById(R.id.compare_prompt_first) ;
        secondPrompt = v.findViewById(R.id.compare_prompt_second) ;
        selectFirst = v.findViewById(R.id.compare_select_first) ;
        actionCompare = v.findViewById(R.id.compare_action_compare) ;
        selectSecond= v.findViewById(R.id.compare_select_second) ;
        bothSelected= v.findViewById(R.id.compare_isbooth_selected) ;

        selectFirst.setOnClickListener(this) ;
        selectSecond.setOnClickListener(this) ;
        actionCompare.setOnClickListener(this);

        return v ;
    }

    @Override
    public void onClick(View view) {
        int callerId = view.getId() ;
        if(callerId == R.id.compare_select_first ){

            Intent intent = new Intent(getContext(), SearchView.class);
            startActivityForResult(intent , FIRST_DEVICE_SELECT_REQUEST_CODE);

        } else if (callerId == R.id.compare_select_second ){

            Intent intent = new Intent(getContext(), SearchView.class);
            startActivityForResult(intent , SECOND_DEVICE_SELECT_REQUEST_CODE);

        } else if ( callerId == R.id.compare_action_compare){

            if(firstDeviceId != null && secondDeviceId != null ){

                Intent intent = new Intent(getActivity(), CompareActivity.class ) ;
                intent.putExtra(FIRST_SELECTED_DEVICE_ID , firstDeviceId) ;
                intent.putExtra(SECOND_SELECTED_DEVICE_ID , secondDeviceId) ;
                intent.putExtra(FIRST_SELECTED_DEVICE_NAME, firstDeviceName) ;
                intent.putExtra(SECOND_SELECTED_DEVICE_NAME, secondDeviceName) ;
                startActivity(intent);

            } else {
                Toast.makeText(getContext() , "please Select two valid Devices !" , Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if( data == null){
            return ;
        }

        if( requestCode == FIRST_DEVICE_SELECT_REQUEST_CODE ){
            firstDeviceId = data.getIntExtra(SEARCHED_DEVICE_ID , -1 ) ;
            firstSelected.setImageResource(R.drawable.check_circle);
            firstDeviceName = data.getStringExtra(SEARCHED_DEVICE_NAME) ;
            firstPrompt.setText(firstDeviceName);

        } else if (requestCode == SECOND_DEVICE_SELECT_REQUEST_CODE){

            secondDeviceId = data.getIntExtra(SEARCHED_DEVICE_ID , -1 ) ;
            secondSelected.setImageResource(R.drawable.check_circle);
            secondDeviceName = data.getStringExtra(SEARCHED_DEVICE_NAME) ;
            secondPrompt.setText(secondDeviceName);

        } else {

        }

    }

}
