package com.example.mobology.compare;

import android.graphics.Bitmap;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;

import org.javatuples.Pair;
import org.javatuples.Triplet;

import java.util.List;

import rx.Observable;

public interface CompareMVP {

    interface View {
        void showToastMessage(String message) ;
        void updateData(List<Triplet<String , String  ,String >> compareDetails ) ;
        void updateDataWithImages(Pair < Pair <Bitmap, Bitmap > , List<Triplet<String , String , String >>> compareDetails ) ;

    }
    interface Model {
        Observable<Pair<DeviceEntry , DeviceEntry>> getTwoDevices(Integer firstDeviceId , Integer secondDeviceId) ;
        Observable<Pair<DeviceEntryWithImage, DeviceEntryWithImage>> getTwoDevicesWithImage(Integer firstDeviceId , Integer secondDeviceId) ;
        // TODO remove the context and inject it
    }
    interface Presenter {
        void rxUnsubscribe();
        void requestDetails(Integer firstDeviceId , Integer secondDeviceId) ;
        void setView(CompareMVP.View view) ;
    }
}
