package com.example.mobology.compare;

import com.example.mobology.Repository.Repository;
import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;

import org.javatuples.Pair;

import java.util.List;

import rx.Observable;
import rx.functions.Func2;

public class CompareModel implements CompareMVP.Model {

    private Repository repository;

    public CompareModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<Pair<DeviceEntry, DeviceEntry>> getTwoDevices(Integer firstDeviceId, Integer secondDeviceId) {
        return Observable.zip(repository.getDevice(firstDeviceId),
                repository.getDevice(secondDeviceId),
                new Func2<List<DeviceEntry>, List<DeviceEntry>, Pair<DeviceEntry, DeviceEntry>>() {
            @Override
            public Pair<DeviceEntry, DeviceEntry>
                call(List<DeviceEntry> deviceEntries, List<DeviceEntry> deviceEntries2) {
                return new Pair<> (deviceEntries.get(0) , deviceEntries2.get(0)) ;

            }
        }) ;
    }

    @Override
    public Observable<Pair<DeviceEntryWithImage, DeviceEntryWithImage>> getTwoDevicesWithImage(Integer firstDeviceId, Integer secondDeviceId) {
        return Observable.zip(repository.getDeviceWithImage(firstDeviceId),
                repository.getDeviceWithImage(secondDeviceId), new Func2<DeviceEntryWithImage, DeviceEntryWithImage, Pair<DeviceEntryWithImage, DeviceEntryWithImage>>() {
                    @Override
                    public Pair<DeviceEntryWithImage, DeviceEntryWithImage> call(DeviceEntryWithImage deviceEntryWithImage, DeviceEntryWithImage deviceEntryWithImage2) {
                        return new Pair<>(deviceEntryWithImage, deviceEntryWithImage2);
                    }
                });
         }
    }
