package com.example.mobology.compare;

import com.example.mobology.Repository.AppDatabase;
import com.example.mobology.Repository.Repository;
import com.example.mobology.Repository.RetrofitClient;

import dagger.Module;
import dagger.Provides;

@Module
public class CompareModule {

    @Provides
    public CompareMVP.Model proviedCompareModel(Repository repository){
        return new CompareModel(repository) ;
    }
    @Provides
    public CompareMVP.Presenter provideComparePresenter(CompareMVP.Model model){
        return new ComparePresenter(model) ;
    }

}
