package com.example.mobology.compare;


import android.graphics.Bitmap;
import android.util.Log;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;

import org.javatuples.Pair;
import org.javatuples.Triplet;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ComparePresenter implements CompareMVP.Presenter {

    private static final String TAG = "ComparePresenter";

    CompareMVP.Model model ;
    CompareMVP.View view ;
    Subscription subscription = null ;

    public ComparePresenter(CompareMVP.Model model) {
        this.model = model;
    }

    @Override
    public void rxUnsubscribe() {
        if(subscription != null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void requestDetails(Integer firstDeviceId, Integer secondDeviceId) {

        subscription = model.getTwoDevicesWithImage(firstDeviceId , secondDeviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Pair<DeviceEntryWithImage, DeviceEntryWithImage>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null){
                            view.showToastMessage("Done :) ");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null ){
                            view.showToastMessage("error loading ): ");
                        }
                    }

                    @Override
                    public void onNext(Pair<DeviceEntryWithImage, DeviceEntryWithImage> objects) {
                            if(view != null){
                            view.updateDataWithImages(objectsToArrayWithImage(objects)) ;
                        }
                    }
                });

//        subscription = model.getTwoDevices(firstDeviceId , secondDeviceId)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<Pair<DeviceEntry, DeviceEntry>>() {
//                    @Override
//                    public void onCompleted() {
//                        if(view != null){
//                            view.showToastMessage("Done :) ");
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if(view != null ){
//                            view.showToastMessage("error loading ): ");
//                        }
//                    }
//
//                    @Override
//                    public void onNext(Pair<DeviceEntry, DeviceEntry> objects) {
//                        if(view != null){
//                            view.updateData(objectsToArray(objects)) ;
//                        }
//                    }
//                }) ;
    }

    @Override
    public void setView(CompareMVP.View view) {
        this.view = view ;
    }

    private List<Triplet<String , String , String >> objectsToArray(Pair<DeviceEntry , DeviceEntry> objects){
        List<Triplet<String, String, String>> result = new ArrayList<>();
        Field[] firstDevicesFields = objects.getValue0().getClass().getDeclaredFields();
        Field[] secondDevicesFields = objects.getValue1().getClass().getDeclaredFields();

        for (int i = 0; i < firstDevicesFields.length; i++) {

            firstDevicesFields[i].setAccessible(true);
            secondDevicesFields[i].setAccessible(true);
            Object value1, value2;
            String name = "";
            try {
                value1 = firstDevicesFields[i].get(objects.getValue0());
                value2 = secondDevicesFields[i].get(objects.getValue1());
                name = secondDevicesFields[i].getName();

            } catch (IllegalAccessException e) {
                value1 = value2 = "Don't Exist";
            }
            if (value1 == null || value2 == null) {
                value1 = value2 = "Don't Exist";
            }
            if (name == null || name.equals("id") || name.equals("name") || name.equals("isFavorite")) {
                continue;
            }
            Log.d(TAG, "objectsToArray: " + name + " = " + value1 + " , " + value2);
            result.add(new Triplet<>(name, "" + value1, "" + value2));
        }
        return result ;
    }

    private Pair < Pair < Bitmap , Bitmap > , List<Triplet<String , String , String >>> objectsToArrayWithImage(Pair<DeviceEntryWithImage , DeviceEntryWithImage > objects){
        List<Triplet<String, String, String>> result = new ArrayList<>();
        Field[] firstDevicesFields = objects.getValue0().deviceEntry.getClass().getDeclaredFields();
        Field[] secondDevicesFields = objects.getValue1().deviceEntry.getClass().getDeclaredFields();

        for (int i = 0; i < firstDevicesFields.length; i++) {

            firstDevicesFields[i].setAccessible(true);
            secondDevicesFields[i].setAccessible(true);
            Object value1, value2;
            String name = "";
            try {
                value1 = firstDevicesFields[i].get(objects.getValue0().deviceEntry);
                value2 = secondDevicesFields[i].get(objects.getValue1().deviceEntry);
                name = secondDevicesFields[i].getName();

            } catch (IllegalAccessException e) {
                value1 = value2 = "Don't Exist";
            }
            if (value1 == null || value2 == null) {
                value1 = value2 = "Don't Exist";
            }
            if (name == null || name.equals("id") || name.equals("name") || name.equals("isFavorite")) {
                continue;
            }
            Log.d(TAG, "objectsToArray: " + name + " = " + value1 + " , " + value2);
            result.add(new Triplet<>(name, "" + value1, "" + value2));
        }
        Pair < Bitmap , Bitmap > images = new Pair <>(objects.getValue0().image , objects.getValue1().image) ;
        return  new Pair <>  ( images, result )  ;

    }
}
