package com.example.mobology.details;


import android.content.Context;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;

import rx.Observable;

public interface DetailsMVP {

    interface View {
        void showToastMessage(String message) ;
        void updateDetails(DeviceEntry DeviceEntry) ;
        void updateDetailsWithImage(DeviceEntryWithImage deviceEntryWithImage) ;
        void setFavorite(Boolean favorite) ;
    }
    interface Model {
        Observable<DeviceEntry> getDetails(Integer deviceId) ;
        Observable<DeviceEntryWithImage> getDetailsWithImage(final Integer deviceId);
        // TODO remove the context and inject it
        void removeFromDataBase(Context context , DeviceEntry deviceEntry) ;
        void addToDataBase(Context context , DeviceEntry deviceEntry) ;
        void setPresenter(DetailsMVP.Presenter presenter) ;
    }
    interface Presenter {
        void rxUnsubscribe();
        void addToFavorite(Context context , DeviceEntry deviceEntry) ;
        void removeFromFavorite(Context context , DeviceEntry deviceEntry) ;
        void notifyDoneDBOperations();
        void requestDetails(Integer deviceId) ;
        void setView(DetailsMVP.View view) ;
    }
}
