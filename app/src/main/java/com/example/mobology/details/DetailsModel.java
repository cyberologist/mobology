package com.example.mobology.details;

import android.content.Context;
import android.util.Log;

import com.example.mobology.Repository.Repository;
import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;


import java.util.List;

import rx.Observable;
import rx.functions.Func1;

public class DetailsModel implements DetailsMVP.Model ,
        Repository.QueryOperationsEventsListener {

    private static final String TAG = "DetailsModel";

    private DetailsMVP.Presenter presenter ;
    private Repository repository;

    public DetailsModel(Repository repository ) {
        this.repository= repository ;
    }

    @Override
    public void setPresenter(DetailsMVP.Presenter presenter) {
        this.presenter = presenter ;
    }

    @Override
    public Observable<DeviceEntry> getDetails(final Integer deviceId) {
        Log.d(TAG, "getDetails: requested ");
        return repository.getDevice(deviceId).flatMap(new Func1<List<DeviceEntry>, Observable<DeviceEntry>>() {
            @Override
            public Observable<DeviceEntry> call(List<DeviceEntry> deviceEntries) {
               if(deviceEntries == null || deviceEntries.size() == 0 ){
                   return Observable.error(new Throwable("devices are empty , there is no such device")) ;
               } else {
                   return Observable.just(deviceEntries.get(0)) ;
               }
            }
        }) ;
    }

    public Observable<DeviceEntryWithImage> getDetailsWithImage(final Integer deviceId) {
       return repository.getDeviceWithImage(deviceId) ;
    }


    @Override
    public void removeFromDataBase( Context context,  DeviceEntry deviceEntry) {
        repository.removeFromDatabase(this , context , deviceEntry);
    }

    @Override
    public void addToDataBase(Context context, DeviceEntry deviceEntry) {
        repository.addToDatabase(this , context , deviceEntry);
    }

    @Override
    public void notifyDone() {
        presenter.notifyDoneDBOperations();
    }
}
