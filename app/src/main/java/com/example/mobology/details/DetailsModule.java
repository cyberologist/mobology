package com.example.mobology.details;

import com.example.mobology.Repository.AppDatabase;
import com.example.mobology.Repository.Repository;
import com.example.mobology.Repository.RetrofitClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailsModule {
    @Provides
    public DetailsMVP.Presenter provideDetailsActivityPresenter(DetailsMVP.Model detailsModel){
        return  new DetailsPresenter(detailsModel) ;
    }
    @Provides
    public DetailsMVP.Model provideDetailsActivityModel(Repository repository){
        return new DetailsModel(repository) ;
    }

}
