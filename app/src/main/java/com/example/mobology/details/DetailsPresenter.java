package com.example.mobology.details;

import android.content.Context;
import android.util.Log;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DetailsPresenter implements DetailsMVP.Presenter {

    private static final String TAG = "DetailsPresenter";

    private DetailsMVP.Model model ;
    private DetailsMVP.View view ;
    private Subscription subscription = null ;

    private Boolean isaddToFavBinding = null ;


    public DetailsPresenter(DetailsMVP.Model model) {
        this.model = model;
        this.model.setPresenter(this) ;
    }

    @Override
    public void setView(DetailsMVP.View view) {
        this.view = view ;
    }

    @Override
    public void addToFavorite(Context context, DeviceEntry deviceEntry) {
        Log.d(TAG, "addToFavorite: called");
        isaddToFavBinding = true ;
        model.addToDataBase(context , deviceEntry);
    }

    @Override
    public void removeFromFavorite(Context context, DeviceEntry deviceEntry) {
        Log.d(TAG, "removeFromFavorite: called");
        isaddToFavBinding = false ;
        model.removeFromDataBase(context , deviceEntry);
    }

    @Override
    public void rxUnsubscribe() {
        if(subscription != null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void requestDetails(Integer deviceId) {

        Log.d(TAG, "requestDetails: registered the subscription");
        model.getDetailsWithImage(deviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DeviceEntryWithImage>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: loading done");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view != null){
                            view.showToastMessage("error loading");
                        }
                    }

                    @Override
                    public void onNext(DeviceEntryWithImage deviceEntryWithImage) {
                        Log.d(TAG, "onNext: called ") ;
                        if(view != null){
                            view.showToastMessage("loading complete");
                            view.updateDetailsWithImage(deviceEntryWithImage);
                        }
                    }
                }) ;
        /*
        model.getDetails(deviceId)

                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DeviceEntry>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: loading done");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view != null){
                            view.showToastMessage("error loading");
                        }
                    }

                    @Override
                    public void onNext(DeviceEntry deviceEntry) {
                        Log.d(TAG, "onNext: called ");
                        if(view != null){
                            view.showToastMessage("loading complete");
                            view.updateDetails(deviceEntry);
                        }
                    }
                });
                */
    }

    @Override
    public void notifyDoneDBOperations() {
        if(view != null){
            view.setFavorite(isaddToFavBinding) ;
        }
    }
}
