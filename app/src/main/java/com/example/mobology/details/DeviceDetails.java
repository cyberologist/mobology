package com.example.mobology.details;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.R;
import com.example.mobology.models.DeviceEntryWithImage;
import com.example.mobology.root.App;
import javax.inject.Inject;

import androidx.appcompat.widget.Toolbar;

import static com.example.mobology.utils.consts.INTENT_DEVICE_ID;
import static com.example.mobology.utils.consts.INTENT_DEVICE_NAME;


public class DeviceDetails extends AppCompatActivity implements DetailsMVP.View {

    private static final String TAG = "DeviceDetails";

    @Inject
    DetailsMVP.Presenter presenter ;

    DeviceEntry deviceEntry ;

    ImageView deviceImg ;
    Toolbar deviceName ;
    ProgressBar progressBar;
    NestedScrollView nestedScrollView ;
    RecyclerView recyclerView ;
    MenuItem favMenuItem ;

    Integer deviceId  ;
    Boolean clicked = null ;
    Boolean errorFavorite = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).getComponent().inject(this);

        setContentView(R.layout.activity_device_details);

        deviceName = findViewById(R.id.device_details_device_name) ;
        deviceImg = findViewById(R.id.device_details_image) ;
        progressBar = findViewById(R.id.device_details_loading_bar) ;
        nestedScrollView = findViewById(R.id.device_details_nested_scroll_view);
        recyclerView = findViewById(R.id.device_details_attrs) ;


        Intent myStarter = getIntent();

        setSupportActionBar(deviceName);
        if(getSupportActionBar() != null ){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (myStarter != null && myStarter.hasExtra(INTENT_DEVICE_NAME)) {
            if(getSupportActionBar() != null){
                getSupportActionBar().setTitle(myStarter.getStringExtra(INTENT_DEVICE_NAME));
            }
        }

        if (myStarter != null && myStarter.hasExtra(INTENT_DEVICE_ID)) {
            deviceId = myStarter.getIntExtra(INTENT_DEVICE_ID, 1);
        }

        Log.d(TAG, "onCreate: " + deviceId );

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        Log.d(TAG, "onResume: requesting..");

        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.device_details_menu , menu);
        favMenuItem = menu.findItem(R.id.add_to_fav) ;
        Log.d(TAG, "onCreateOptionsMenu: favmenuItem : " + favMenuItem);
        presenter.requestDetails(deviceId);
        return true ;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int callerId = item.getItemId() ;
        Log.d(TAG, "onOptionsItemSelected: caller id = " + callerId );
        if( callerId == R.id.add_to_fav){

            Log.d(TAG, "onOptionsItemSelected: called");

            if(clicked == null || deviceEntry == null ){
                Toast.makeText(getApplicationContext() , "please wait .." , Toast.LENGTH_SHORT).show() ;
            } else if(errorFavorite != null ) {
                Toast.makeText(getApplicationContext() , "an error occured , try again later.." , Toast.LENGTH_SHORT).show() ;
            } else {
                switchClicked();
            }
                return true ;

        } else if ( callerId == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void switchClicked(){

        Log.d(TAG, "switchClicked: clicked = " + clicked);
            if(!clicked) {
                presenter.addToFavorite(getApplicationContext(), deviceEntry ) ;
            } else {
                presenter.removeFromFavorite(getApplicationContext() , deviceEntry ) ;
            }
            clicked = null ;
    }

    @Override
    public void setFavorite(Boolean favorite) {
        Log.d(TAG, "setFavorite: done " + favorite);
        if( favorite == null){
            errorFavorite = true ;
            return ;
        }
        favMenuItem.setIcon( favorite
                ? getDrawable(R.drawable.selected_fav)
                : getDrawable(R.drawable.unselected_fav)  );
        this.clicked = favorite ;
    }

    @Override
    public void showToastMessage(String message) {
        Log.d(TAG, "showToastMessage: called");
        Toast.makeText(this , message , Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateDetails(DeviceEntry deviceEntry) {

        Log.d(TAG, "updateDetails: called ");
        progressBar.setVisibility(View.INVISIBLE);
        this.deviceEntry = deviceEntry ;
        if(deviceEntry != null){
            clicked = deviceEntry.getFavorite() ;
            Log.d(TAG, "updateDetails: clicked = " + clicked );
            favMenuItem.setIcon( clicked
                    ? getDrawable(R.drawable.selected_fav)
                    : getDrawable(R.drawable.unselected_fav)  );

            Log.d(TAG, "updateDetails: " + deviceEntry.getName());
        } else {
            Log.d(TAG, "updateDetails: device entry is null");
        }

        DeviceDetailsAdapter mAdapter = new DeviceDetailsAdapter(deviceEntry , DeviceDetails.this ) ;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this) ;
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);

    }

    @Override
    public void updateDetailsWithImage(DeviceEntryWithImage deviceEntryWithImage) {
        Log.d(TAG, "updateDetailsWithImage : called ");
        progressBar.setVisibility(View.INVISIBLE);
        this.deviceEntry = deviceEntryWithImage.deviceEntry ;
        if(deviceEntry != null){
            clicked = deviceEntry.getFavorite() ;
            Log.d(TAG, "updateDetails: clicked = " + clicked );
            favMenuItem.setIcon( clicked
                    ? getDrawable(R.drawable.selected_fav)
                    : getDrawable(R.drawable.unselected_fav)  );

            Log.d(TAG, "updateDetails: " + deviceEntry.getName());
        } else {
            Log.d(TAG, "updateDetails: device entry is null");
        }

        DeviceDetailsAdapter mAdapter = new DeviceDetailsAdapter(deviceEntry , DeviceDetails.this ) ;
        LinearLayoutManager layoutManager = new LinearLayoutManager(this) ;
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);
        deviceImg.setImageBitmap(deviceEntryWithImage.image);
    }
}
