package com.example.mobology.details;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mobology.R;
import com.example.mobology.models.DeviceEntry;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DeviceDetailsAdapter extends RecyclerView.Adapter<DeviceDetailsAdapter.TextViewHolder> {

    private static final String TAG = "DeviceDetailsAdapter";

    DeviceEntry deviceEntry ;
    List<Pair< String , String > > content ;
    Context context ;

    public DeviceDetailsAdapter(DeviceEntry deviceEntry , Context context) {
        this.deviceEntry = deviceEntry;
        this.context = context;

        if( deviceEntry == null ){
            content = null ;
        } else {
            this.content = ClassToArray(deviceEntry , context) ;
        }

    }

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = ((Activity) context).getLayoutInflater().inflate(R.layout.device_atrr_item , parent , false) ;
        return new TextViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        if(content == null || content.size() == 0 ){
            holder.attrName.setText("Sorry an error occured");
            holder.attr.setText("try again later");
        } else {
            holder.attrName.setText(content.get(position).first + " : ");
            holder.attr.setText(content.get(position).second);
        }
    }

    @Override
    public int getItemCount() {
        return content == null || content.size() == 0 ? 1 : content.size();
    }
    public void refreshContentWith(List<Pair<String , String >> content){
        Log.d(TAG, "refreshContentWith: called");
        this.content = content ;
        notifyDataSetChanged();
        for( Pair<String , String> s : content){
            Log.d(TAG, "refreshContentWith: " + s.first);
        }
    }
    public class TextViewHolder extends RecyclerView.ViewHolder {
        TextView attrName ;
        TextView attr ;
        public TextViewHolder(@NonNull View itemView) {
            super(itemView);
            attrName = itemView.findViewById(R.id.device_details_item_attr_name) ;
            attr = itemView.findViewById(R.id.device_details_item_attr) ;
        }
    }
    private List<Pair<String , String >> ClassToArray(DeviceEntry deviceEntry , Context context){

        if( deviceEntry == null){
            Log.wtf(TAG, "ClassToArray: null");
            return null ;
        }

        List<Pair<String , String >> result = new ArrayList<>();


        Field[] fields = deviceEntry.getClass().getDeclaredFields();

        if(fields.length == 0 ){
            Log.wtf("sssss" , "ffffffff") ;
        }

        for(Field f : fields){
            f.setAccessible(true);
            Object value ;
            String name = " " ;
            try{
                value = f.get(deviceEntry) ;
                name = f.getName() ;

            } catch (IllegalAccessException e) {
                value = "Don't Exist" ;
            }
            if(value == null) {
                value = "Don't Exist" ;
            }
            if(name == null || name.equals("id") || name.equals("name") || name.equals("isFavorite")){
                continue;
            }
            Log.d(TAG, "ClassToArray: " + name + " = " + value);
            result.add(new Pair<>(name,"" + value )) ;
        }
        return result ;
    }
}