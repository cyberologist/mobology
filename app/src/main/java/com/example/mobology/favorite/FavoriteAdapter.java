package com.example.mobology.favorite;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mobology.R;
import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.ResultEntry;
import com.example.mobology.details.DeviceDetails;

import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.example.mobology.utils.consts.INTENT_DEVICE_ID;
import static com.example.mobology.utils.consts.INTENT_DEVICE_NAME;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ResultViewHolder> {

    private static final String TAG = "FavoriteAdapter";

    public Context context ;
    public List<ResultEntry> content ;

    public FavoriteAdapter(Context context , List<ResultEntry> content , List<DeviceEntry> eContent  ) {
        this.content = content;
        this.context = context;
    }

    @NonNull
    @Override
    public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent , false ) ;
        return new ResultViewHolder(v) ;
    }

    @Override
    public void onBindViewHolder(@NonNull ResultViewHolder holder, final int position) {

        if (content == null ){

            holder.mainName.setText("           Loading you Favorite Devices :)");
            holder.subName.setText("                  Please waite :D ....");
            holder.resItem.setClickable(false);

        } else if (content.size() == 0 ){
            holder.mainName.setText("           looks like you don't have any !");
            holder.subName.setText("                       browse other Devices and add whatever you like :)");
            holder.resItem.setClickable(false);
        } else {

            holder.mainName.setText(content.get(position).getMainName());
            holder.subName.setText(content.get(position).getSubName());
            holder.resItem.setClickable(true);

            holder.resItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DeviceDetails.class);
                    intent.putExtra(INTENT_DEVICE_ID, content.get(position).getId());
                    intent.putExtra(INTENT_DEVICE_NAME, content.get(position).getMainName());
                    context.startActivity(intent);
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return content == null || content.size() == 0 ? 1 : content.size() ;
    }

    public List<ResultEntry> getContent() {
        return content;
    }

    class ResultViewHolder extends  RecyclerView.ViewHolder {
        TextView mainName ;
        TextView subName ;
        LinearLayout resItem ;
        private ResultViewHolder(View v){
            super(v) ;
            mainName = v.findViewById(R.id.search_main_name) ;
            subName = v.findViewById(R.id.search_sub_name) ;
            resItem = v.findViewById(R.id.search_item) ;

        }

    }
    void push(DeviceEntry deviceEntry){
        if(content == null){
            content = new LinkedList<>() ;
        }
        content.add(new ResultEntry(deviceEntry.getName() ,
                deviceEntry.getBrand() ,
                deviceEntry.getId() ,
                deviceEntry.getImageUrl())) ;

//        notifyItemInserted(content.size() - 1 );
        notifyDataSetChanged();
    }
    void remove(int position){
        content.remove(position) ;
//        notifyItemRemoved(position);
        notifyDataSetChanged();
    }
}
