package com.example.mobology.favorite;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.mobology.models.ResultEntry;
import com.example.mobology.root.App;
import com.example.mobology.models.DeviceEntry;
import com.example.mobology.R;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class FavoriteFragment extends Fragment  implements FavoriteMVP.View {

    private FavoriteAdapter mAdapter ;
    private List<DeviceEntry> deviceEntriesContent;

    @Inject
    public FavoriteMVP.Presenter presenter ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setRetainInstance(true);
        ((App) getActivity().getApplication()).getComponent().inject(this);

        Toolbar mTopToolbar ;
        View v = inflater.inflate(R.layout.fragment_fav, container , false) ;
        mTopToolbar = v.findViewById(R.id.my_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mTopToolbar);

        RecyclerView recyclerView ;

        ActionBar acb = ((((AppCompatActivity)getActivity()))).getSupportActionBar() ;
        if(acb != null){
            acb.setDisplayHomeAsUpEnabled(true);
        }

        DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, mTopToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mAdapter = new FavoriteAdapter(getContext() , null , null ) ;

        recyclerView = v.findViewById(R.id.favorite_devices_list) ;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
        final Context context = getContext() ;
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0 , ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                presenter.removeFromFav(context ,deviceEntriesContent.get(viewHolder.getAdapterPosition() ));
                mAdapter.remove(viewHolder.getAdapterPosition());
            }
        }).attachToRecyclerView(recyclerView);
        deviceEntriesContent = new ArrayList<>() ;

        presenter.setView(this) ;
        presenter.requestAllDevices();
        return v ;
    }

    @Override
    public void setDevices(List<DeviceEntry> deviceEntries) {
        deviceEntriesContent = new ArrayList<>() ;
        List<ResultEntry> ls = new ArrayList<>();
        for( DeviceEntry d : deviceEntries){
            ls.add(new ResultEntry(d.getName() , d.getBrand() , d.getId() , d.getImageUrl()));
            deviceEntriesContent.add(d);
        }
        mAdapter.content = ls ;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.observerUnsubscribe();
    }

    @Override
    public void showToastMessage(String text) {
        Toast.makeText(this.getContext(), "" + text , Toast.LENGTH_LONG).show();
    }
    @Override
    public void addToDevices(DeviceEntry device) {
        if(device != null){
            mAdapter.push(device) ;
            deviceEntriesContent.add(device);
        }
    }
}
