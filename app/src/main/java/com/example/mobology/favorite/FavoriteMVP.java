package com.example.mobology.favorite;

import android.content.Context;

import com.example.mobology.Repository.Repository;
import com.example.mobology.models.DeviceEntry;

import java.util.List;

import androidx.lifecycle.LiveData;

public interface FavoriteMVP {
    interface View {
        void showToastMessage(String text) ;
        void addToDevices(DeviceEntry devices) ;
        void setDevices(List<DeviceEntry> deviceEntries) ;
    }
    interface Presenter {
        void removeFromFav(Context context , DeviceEntry deviceEntry) ;
        void requestAllDevices();
        void getAllDevices(LiveData<List<DeviceEntry>> deviceEntries);
        void setView(FavoriteMVP.View view) ;
        void observerUnsubscribe();
    }
    interface Model {
        void setPresenter(FavoriteMVP.Presenter presenter) ;
        void queryAllDevices() ;
        void removeFromFav(Context context , DeviceEntry deviceEntry) ;
    }
}
