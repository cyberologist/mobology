package com.example.mobology.favorite;

import android.content.Context;

import com.example.mobology.Repository.LocalRepository;
import com.example.mobology.Repository.Repository;
import com.example.mobology.models.DeviceEntry;

import java.util.List;

import androidx.lifecycle.LiveData;

public class FavoriteModel implements FavoriteMVP.Model ,
        Repository.AsyncQueryGetter {

    FavoriteMVP.Presenter presenter ;
    Repository repository ;

    public FavoriteModel(Repository repository) {
        this.repository = repository;
    }

    public void setPresenter(FavoriteMVP.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void queryAllDevices() {
        repository.requestAllDevices(this);
    }

    @Override
    public void removeFromFav(Context context, DeviceEntry deviceEntry) {
        repository.removeFromDatabase(context , deviceEntry);
    }


    @Override
    public void returnedQuery(LiveData<List<DeviceEntry>> deviceEntries) {
        presenter.getAllDevices(deviceEntries);
    }
}
