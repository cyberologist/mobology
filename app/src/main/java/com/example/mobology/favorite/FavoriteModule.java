package com.example.mobology.favorite;

import com.example.mobology.Repository.AppDatabase;
import com.example.mobology.Repository.LocalRepository;
import com.example.mobology.Repository.Repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class FavoriteModule {

    @Provides
    public FavoriteMVP.Presenter provideDetailsActivityPresenter(FavoriteMVP.Model favoriteModel){
        return  new FavoritePresenter(favoriteModel) ;
    }
    @Provides
    public FavoriteMVP.Model provideDetailsActivityModel(Repository repository){
        return new FavoriteModel(repository) ;
    }

}
