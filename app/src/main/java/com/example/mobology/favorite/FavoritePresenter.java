package com.example.mobology.favorite;

import android.content.Context;

import com.example.mobology.Repository.Repository;
import com.example.mobology.models.DeviceEntry;

import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import io.reactivex.disposables.Disposable;


public class FavoritePresenter implements FavoriteMVP.Presenter {

    FavoriteMVP.Model model ;
    FavoriteMVP.View view ;
    Disposable disposable = null ;
    Observer observer ;
    LiveData<List<DeviceEntry>> nowData ;
    public FavoritePresenter(FavoriteMVP.Model model) {
        this.model = model;
        model.setPresenter(this);
    }

    @Override
    public void setView(FavoriteMVP.View view) {
        this.view = view ;
    }

    @Override
    public void getAllDevices(LiveData<List<DeviceEntry>> deviceEntries) {
        nowData = deviceEntries ;
        observer = new Observer<List<DeviceEntry>>() {
            @Override
            public void onChanged(List<DeviceEntry> deviceEntries) {
                if(view != null){
                    view.setDevices(deviceEntries);
                }
            }
        } ;
        deviceEntries.observe((LifecycleOwner)view,new Observer<List<DeviceEntry>>() {
            @Override
            public void onChanged(List<DeviceEntry> deviceEntries) {
                if(view != null){
                    view.setDevices(deviceEntries);
                }
            }
        });
    }

    @Override
    public void requestAllDevices() {
        model.queryAllDevices();
    }

    @Override
    public void observerUnsubscribe() {
        if(nowData != null){
            if(nowData.hasObservers()){
                nowData.removeObserver(observer);
            }
        }

    }

    @Override
    public void removeFromFav(Context context , DeviceEntry deviceEntry) {
        model.removeFromFav(context , deviceEntry);
    }
}
