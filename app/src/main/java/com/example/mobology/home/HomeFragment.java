package com.example.mobology.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.R;
import com.example.mobology.models.DeviceEntryWithImage;
import com.example.mobology.root.App;
import com.example.mobology.search.SearchView;
import com.rd.PageIndicatorView;


import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

public class HomeFragment extends Fragment implements HomeMVP.View{

    private static final String TAG = "HomeFragment";
    public static final String AM_I_A_MAIN_SEARCH_ACTIVITY = "aiamsa" ;

    @Inject
    HomeMVP.Presenter presenter ;

    private HomePagerAdapter mAdapter;

    private View placeholder ;
    private ImageView placeHolderImage ;
    private TextView placeHolderName ;
    private TextView placeHolderBrand ;

    private int num = 0 ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getActivity() != null){
            ((App) getActivity().getApplication()).getComponent().inject(this);
        } else {
            Log.wtf(TAG , "activity in null ): ") ;
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setRetainInstance(true);

        PageIndicatorView pb ;
        ViewPager homePager;
        ImageView openSearchActivity;
        ImageView hamburger;


        final View v = inflater.inflate(R.layout.fragment_home, container, false);

        homePager = v.findViewById(R.id.home_pager);

        hamburger = v.findViewById(R.id.hamburger);
        hamburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: called");
                if (getActivity() != null) {
                    final DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);
                    drawer.openDrawer(Gravity.LEFT);
                }
            }
        });

        openSearchActivity = v.findViewById(R.id.open_search_activity);
        openSearchActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SearchView.class);
                intent.putExtra(AM_I_A_MAIN_SEARCH_ACTIVITY , true ) ;
                startActivity(intent);
            }
        });

        pb = v.findViewById(R.id.pageIndicatorView) ;
        pb.setDynamicCount(true);

        placeholder = v.findViewById(R.id.home_placeholder) ;
        placeHolderImage = placeholder.findViewById(R.id.home_device_image);
        placeHolderImage.setScaleType(ImageView.ScaleType.CENTER);
        placeHolderName = placeholder.findViewById(R.id.home_device_name);
        placeHolderBrand = placeholder.findViewById(R.id.home_device_desc);
        Button placeHolderButton = placeholder.findViewById(R.id.home_device_details_button);

        placeholder.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        placeHolderName.setText("Loading...") ;
        placeHolderBrand.setText("Please Wait :)");
        placeHolderButton.setVisibility(View.GONE);
        placeHolderImage.setImageResource(R.drawable.ic_launcher_foreground);


        mAdapter = new HomePagerAdapter(getContext() , homePager);
        homePager.setAdapter(mAdapter);

        presenter.setView(this);
        presenter.requestHomeItems();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext() , message , Toast.LENGTH_LONG).show();
        placeHolderImage.setImageResource(R.drawable.error_loading);
        placeHolderName.setText("Sorry ): ") ;
        placeHolderBrand.setText("an error occurred , please report , and we will fix it happily :)");

    }

    @Override
    public void addToHome(DeviceEntry deviceEntry) {
        placeholder.setVisibility(View.GONE);
    }

    @Override
    public void addToHomeWithImage(DeviceEntryWithImage deviceEntryWithImage) {
        placeholder.setVisibility(View.GONE);
        mAdapter.addView(deviceEntryWithImage) ;
        Log.d(TAG, "addToHomeWithImage: added item" + (++num) );
    }
}
