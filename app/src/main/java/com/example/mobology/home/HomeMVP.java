package com.example.mobology.home;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;

import rx.Observable;

public interface HomeMVP {

    interface View {
        void showToastMessage(String message) ;
        void addToHome(DeviceEntry DeviceEntry) ;
        void addToHomeWithImage(DeviceEntryWithImage deviceEntryWithImage) ;
    }
    interface Model {
        Observable<DeviceEntry> getHomeItems() ;
        Observable<DeviceEntryWithImage> getHomeItemsWithImage();
    }
    interface Presenter {
        void rxUnsubscribe();
        void requestHomeItems() ;
        void setView(HomeMVP.View view) ;
    }
}
