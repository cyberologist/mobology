package com.example.mobology.home;

import com.example.mobology.Repository.Repository;
import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.DeviceEntryWithImage;


import rx.Observable;

public class HomeModel implements  HomeMVP.Model {

    private Repository repository ;

    public HomeModel(Repository  repository) {
        this.repository = repository;
    }

    @Override
    public Observable<DeviceEntry> getHomeItems() {
        return repository.getRandomItemsFromNetwork();
    }

    @Override
    public Observable<DeviceEntryWithImage> getHomeItemsWithImage() {
        return repository.getRandomItemsFromNetworkWithImages() ;
    }

}
