package com.example.mobology.home;

import com.example.mobology.Repository.Repository;
import com.example.mobology.Repository.RetrofitClient;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

    @Provides
    public HomeMVP.Model provideHomeModel(Repository repository){
        return new HomeModel(repository) ;
    }

    @Provides HomeMVP.Presenter provideHomePresenter(HomeMVP.Model model){
        return new HomePresenter(model) ;
    }

}
