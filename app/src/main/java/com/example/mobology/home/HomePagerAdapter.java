package com.example.mobology.home;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mobology.models.DeviceEntryWithImage;
import com.example.mobology.details.DeviceDetails;
import com.example.mobology.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import static com.example.mobology.utils.consts.INTENT_DEVICE_ID;
import static com.example.mobology.utils.consts.INTENT_DEVICE_NAME;


public class HomePagerAdapter extends PagerAdapter {

    private static final String TAG = "HomePagerAdapter";

    private Context context;
    private List<DeviceEntryWithImage> contentWithImage;
    private ViewGroup container;

    private View gotit[] = new View[15];


    HomePagerAdapter(Context context, ViewGroup container) {
        this.contentWithImage = new ArrayList<>();
        this.context = context;
        this.container = container;
    }

    void addView(DeviceEntryWithImage deviceEntryWithImage) {
        contentWithImage.add(deviceEntryWithImage);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {


        View layout = gotit[position];

        if (layout == null) {

            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            layout = inflater.inflate(R.layout.home_item, container, false);

            // set The Image according to the position
            TextView mainName = layout.findViewById(R.id.home_device_name);
            TextView mainDesc = layout.findViewById(R.id.home_device_desc);
            Button mainButton = layout.findViewById(R.id.home_device_details_button);
            ImageView mainImage = layout.findViewById(R.id.home_device_image);

            mainName.setText(contentWithImage.get(position).deviceEntry.getName());
            mainDesc.setText(contentWithImage.get(position).deviceEntry.getBrand());

            Log.d(TAG, "instantiateItem: image set !!");
            mainImage.setImageBitmap(contentWithImage.get(position).image);

            mainButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DeviceDetails.class);
                    intent.putExtra(INTENT_DEVICE_ID, contentWithImage.get(position).deviceEntry.getId());
                    intent.putExtra(INTENT_DEVICE_NAME, contentWithImage.get(position).deviceEntry.getName());
                    context.startActivity(intent);
                }
            });

            gotit[position] = layout;
        }
        container.addView(layout);
        return layout;
    }

    @Override
    public int getCount() {
        return contentWithImage == null || contentWithImage.size() == 0 ? 0 : contentWithImage.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        Log.d(TAG, "destroyItem: destroyed : " + position);
        container.removeView((View) (object));
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}
