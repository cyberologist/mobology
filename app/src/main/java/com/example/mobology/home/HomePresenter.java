package com.example.mobology.home;

import android.util.Log;

import com.example.mobology.models.DeviceEntryWithImage;

import androidx.fragment.app.Fragment;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.Subscription;
import rx.schedulers.Schedulers;

public class HomePresenter implements HomeMVP.Presenter {

    private static final String TAG = "HomePresenter";

    HomeMVP.Model model ;
    HomeMVP.View view ;

    Subscription subscription ;
    public HomePresenter(HomeMVP.Model model) {
        this.model = model;
    }

    @Override
    public void rxUnsubscribe() {
        if(subscription != null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void requestHomeItems() {
        subscription = model.getHomeItemsWithImage()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DeviceEntryWithImage>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: Done :) ");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: error -> ");
                        e.printStackTrace();
                        if(view != null && (((Fragment)view).getContext())!= null){
                            view.showToastMessage("error loading Home ): ");
                        }
                    }

                    @Override
                    public void onNext(DeviceEntryWithImage deviceEntryWithImage) {
                            if(view != null){
                                view.addToHomeWithImage(deviceEntryWithImage);
                            }
                    }
                }) ;
    }

    @Override
    public void setView(HomeMVP.View view) {
        this.view = view ;
    }
}
