package com.example.mobology.models;


import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "Devices" )
public class DeviceEntry {

    @PrimaryKey()
    private Integer id ;
    @Ignore
    private boolean isFavorite = false ;

    private String createdAt ;
    private String updatedAt ;
    private String name ;
    private String brand ;
    private String edge ;
    private String announced ;
    private String status ;
    private String dimensions ;
    private String weight ;
    private String sim ;
    private String type ;
    private String size ;
    private String resolution ;
    private String wlan ;
    private String bluetooth ;
    private String gps ;
    private String usp ;
    private String battery_c ;
    private String colors ;
    private String sensors ;
    private String cpu ;
    private String internal ;
    private String os ; 
    private String video ;
    private String chipset ;
    private String gpu ;
    private String single ;
    private String triple ;
    private String charging ;
    private String imageUrl ;

    public DeviceEntry(Integer id, String createdAt, String updatedAt, String name, String brand, String edge, String announced, String status, String dimensions, String weight, String sim, String type, String size, String resolution, String wlan, String bluetooth, String gps, String usp, String battery_c, String colors, String sensors, String cpu, String internal, String os, String video, String chipset, String gpu, String single, String triple, String charging , String imageUrl) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.name = name;
        this.brand = brand;
        this.edge = edge;
        this.announced = announced;
        this.status = status;
        this.dimensions = dimensions;
        this.weight = weight;
        this.sim = sim;
        this.type = type;
        this.size = size;
        this.resolution = resolution;
        this.wlan = wlan;
        this.bluetooth = bluetooth;
        this.gps = gps;
        this.usp = usp;
        this.battery_c = battery_c;
        this.colors = colors;
        this.sensors = sensors;
        this.cpu = cpu;
        this.internal = internal;
        this.os = os;
        this.video = video;
        this.chipset = chipset;
        this.gpu = gpu;
        this.single = single;
        this.triple = triple;
        this.charging = charging;
        this.imageUrl = imageUrl ;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getEdge() {
        return edge;
    }

    public String getAnnounced() {
        return announced;
    }

    public String getStatus() {
        return status;
    }

    public String getDimensions() {
        return dimensions;
    }

    public String getWeight() {
        return weight;
    }

    public String getSim() {
        return sim;
    }

    public String getType() {
        return type;
    }

    public String getSize() {
        return size;
    }

    public String getResolution() {
        return resolution;
    }

    public String getWlan() {
        return wlan;
    }

    public String getBluetooth() {
        return bluetooth;
    }

    public String getGps() {
        return gps;
    }

    public String getUsp() {
        return usp;
    }

    public String getBattery_c() {
        return battery_c;
    }

    public String getColors() {
        return colors;
    }

    public String getSensors() {
        return sensors;
    }

    public String getCpu() {
        return cpu;
    }

    public String getInternal() {
        return internal;
    }

    public String getOs() {
        return os;
    }

    public String getVideo() {
        return video;
    }

    public String getChipset() {
        return chipset;
    }

    public String getGpu() {
        return gpu;
    }

    public String getSingle() {
        return single;
    }

    public String getTriple() {
        return triple;
    }

    public String getCharging() {
        return charging;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setEdge(String edge) {
        this.edge = edge;
    }

    public void setAnnounced(String announced) {
        this.announced = announced;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public void setWlan(String wlan) {
        this.wlan = wlan;
    }

    public void setBluetooth(String bluetooth) {
        this.bluetooth = bluetooth;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public void setUsp(String usp) {
        this.usp = usp;
    }

    public void setBattery_c(String battery_c) {
        this.battery_c = battery_c;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public void setSensors(String sensors) {
        this.sensors = sensors;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public void setInternal(String internal) {
        this.internal = internal;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void setChipset(String chipset) {
        this.chipset = chipset;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public void setSingle(String single) {
        this.single = single;
    }

    public void setTriple(String triple) {
        this.triple = triple;
    }

    public void setCharging(String charging) {
        this.charging = charging;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
