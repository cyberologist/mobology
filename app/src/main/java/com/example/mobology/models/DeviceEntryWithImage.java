package com.example.mobology.models;

import android.graphics.Bitmap;

public class DeviceEntryWithImage {
    public DeviceEntry deviceEntry ;
    public Bitmap image ;

    public DeviceEntryWithImage(DeviceEntry deviceEntry, Bitmap image) {
        this.deviceEntry = deviceEntry;
        this.image = image;
    }
}
