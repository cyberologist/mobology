package com.example.mobology.models;

public class ResultEntry{
    String mainName ;
    String subName ;
    Integer id ;
    String imageUrl ;

    public ResultEntry(String mainName, String subName , Integer id , String imageUrl ) {
        this.mainName = mainName;
        this.subName = subName;
        this.id = id ;
        this.imageUrl = imageUrl ;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMainName() {
        return mainName;
    }

    public void setMainName(String mainName) {
        this.mainName = mainName;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}