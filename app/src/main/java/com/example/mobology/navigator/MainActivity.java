package com.example.mobology.navigator;

import android.os.Bundle;
import android.util.Log;

import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.example.mobology.R;
import com.example.mobology.categories.CategoriesFragment;
import com.example.mobology.compare.CompareFragment;
import com.example.mobology.favorite.FavoriteFragment;
import com.example.mobology.root.App;
import com.example.mobology.utils.consts;
import com.example.mobology.home.HomeFragment;
import com.google.android.material.navigation.NavigationView;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import static com.example.mobology.utils.consts.APPLE_FRAGMENT;
import static com.example.mobology.utils.consts.FRAGMENT_TYPE_INDICATOR;
import static com.example.mobology.utils.consts.HAWAWI_FRAGMENT;
import static com.example.mobology.utils.consts.SAMSUNG_FRAGMENT;
import static com.example.mobology.utils.consts.SONY_FRAGMENT;
import static com.example.mobology.utils.consts.TRENDING_FRAGMENT;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private boolean isHomeShowing  ;
    private static final String TAG = "MainActivity";

    NavigationView navigationView;

    SparseArray<Fragment> fragmentMap = new SparseArray<>();

    private View homeFragmentView ;
    private View compareFragmentView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: called");

        ((App) getApplication()).getComponent().inject(this);

        setContentView(R.layout.activity_main);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(MainActivity.this);
        if (savedInstanceState == null) {
            getFragment(consts.HOME_FRAGMENT);
            navigationView.setCheckedItem(R.id.nav_home);
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: called");
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(!isHomeShowing){
                getFragment(consts.HOME_FRAGMENT);
                navigationView.setCheckedItem(R.id.nav_home);
            } else {
                Log.d(TAG, "onBackPressed: super");
                super.onBackPressed();
            }
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Log.d(TAG, "onNavigationItemSelected: ");
        int id = item.getItemId();
        switch (id) {
            case (R.id.nav_home):
                getFragment(consts.HOME_FRAGMENT);
                break ;
            case (R.id.nav_compare):
                Log.d(TAG, "onNavigationItemSelected: compare Pressed");
                getFragment(consts.COMPARE_FRAGMENT);
                break ;
            case R.id.nav_fav:
                getFragment(consts.FAVORITE_FRAGMENT);
                break;
            case (R.id.nav_trending):
                getFragment(consts.TRENDING_FRAGMENT);
                break;
            case (R.id.nav_sony):
                getFragment(consts.SONY_FRAGMENT);
                break;
            case (R.id.nav_samsung):
                getFragment(consts.SAMSUNG_FRAGMENT);
                break;
            case (R.id.nav_apple):
                getFragment(consts.APPLE_FRAGMENT);
                break;
            case (R.id.nav_hawawi):
                getFragment(consts.HAWAWI_FRAGMENT);
                break;
            case (R.id.nav_about_dev):
                break;

            default:
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: is HomeShowing = " + isHomeShowing);
        if(isHomeShowing){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public void getFragment(int fragmentId) {

        isHomeShowing = (fragmentId == consts.HOME_FRAGMENT) ;

        Log.d(TAG, "getFragment: called ");
        boolean addNewFragment = false ;
        if(fragmentMap.get(fragmentId) == null) {
            Log.d(TAG, "getFragment: flllllllll");
            addNewFragment = true;
            Log.e(TAG, "getFragment: recreated");
            switch (fragmentId) {
                case consts.APPLE_FRAGMENT:
                    fragmentMap.put(fragmentId, getAppleFragment());
                    break;
                case consts.SONY_FRAGMENT:
                    fragmentMap.put(fragmentId, getSonyFragment());
                    break;
                case consts.HAWAWI_FRAGMENT:
                    fragmentMap.put(fragmentId, getHawawiFragment());
                    break;
                case consts.TRENDING_FRAGMENT:
                    fragmentMap.put(fragmentId, getTrendingFragment());
                    break;
                case consts.COMPARE_FRAGMENT:
                    Log.d(TAG, "getFragment: called for compare");
                    fragmentMap.put(fragmentId, new CompareFragment());
                    break;
                case consts.HOME_FRAGMENT:
                    Log.d(TAG, "getFragment: trrrrrrrr");
                    fragmentMap.put(fragmentId, new HomeFragment());
                    break;
                case SAMSUNG_FRAGMENT:
                    fragmentMap.put(fragmentId, getSamsungFragment());
                    break;
                case consts.FAVORITE_FRAGMENT:
                    fragmentMap.put(fragmentId, new FavoriteFragment());
                    break;

                default:
                    // TODO what to do ?
                    Log.d(TAG, "getFragment: wtf fragment = null ");
            }
        }


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction() ;
        for( int i = 0 ; i < fragmentMap.size() ; i ++ ){

            Log.e(TAG, "getFragment: fragments at backStack " + fragmentMap.valueAt(i).toString() );

            if(fragmentMap.keyAt(i) == fragmentId){
                continue;
            }
            if(fragmentMap.valueAt(i).isVisible()){
                Log.e(TAG, "getFragment: " + fragmentMap.valueAt(i).toString() + " is not hidden " );
                transaction.hide(fragmentMap.valueAt(i)).commit();
                transaction = getSupportFragmentManager().beginTransaction() ;
            }
        }

        Log.d(TAG, "getFragment: chosen " + fragmentMap.get(fragmentId).toString() );
        if(addNewFragment){
            transaction.add(R.id.content , fragmentMap.get(fragmentId) ).commit();
        } else {
            transaction.show(fragmentMap.get(fragmentId) ).commit();
        }

        Log.d(TAG, "getFragment: home = " + isHomeShowing );
        if(isHomeShowing) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        Log.d(TAG, "getFragment: fragments size " + fragmentMap.size() );
    }



    private Fragment getTrendingFragment() {
            Bundle bundle = new Bundle();
            bundle.putInt(FRAGMENT_TYPE_INDICATOR, TRENDING_FRAGMENT);
            CategoriesFragment categoriesFragment = new CategoriesFragment();
            categoriesFragment.setArguments(bundle);
        return categoriesFragment ;
    }

    private Fragment getSamsungFragment() {
            Bundle bundle = new Bundle();
            bundle.putInt(FRAGMENT_TYPE_INDICATOR, SAMSUNG_FRAGMENT);
            CategoriesFragment categoriesFragment = new CategoriesFragment();
            categoriesFragment.setArguments(bundle);
            return categoriesFragment ;
    }

    private Fragment getSonyFragment() {
        Bundle bundle = new Bundle();
        bundle.putInt(FRAGMENT_TYPE_INDICATOR, SONY_FRAGMENT);
        CategoriesFragment categoriesFragment = new CategoriesFragment();
        categoriesFragment.setArguments(bundle);
        return categoriesFragment ;
    }

    private Fragment getHawawiFragment() {
        Bundle bundle = new Bundle();
        bundle.putInt(FRAGMENT_TYPE_INDICATOR, HAWAWI_FRAGMENT);
        CategoriesFragment categoriesFragment = new CategoriesFragment();
        categoriesFragment.setArguments(bundle);
        return categoriesFragment ;

    }

    private Fragment getAppleFragment() {
        Bundle bundle = new Bundle();
        bundle.putInt(FRAGMENT_TYPE_INDICATOR, APPLE_FRAGMENT);
        CategoriesFragment categoriesFragment = new CategoriesFragment();
        categoriesFragment.setArguments(bundle);
        return categoriesFragment;
    }
}
