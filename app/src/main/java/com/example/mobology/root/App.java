package com.example.mobology.root;

import android.app.Application;

import com.example.mobology.Repository.RepositoryModule;
import com.example.mobology.categories.CategoriesModule;
import com.example.mobology.compare.CompareModule;
import com.example.mobology.details.DetailsModule;
import com.example.mobology.favorite.FavoriteModule;
import com.example.mobology.home.HomeModule;
import com.example.mobology.search.SearchModule;

public class App extends Application {
    public ApplicationComponent component ;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .repositoryModule(new RepositoryModule())
                .searchModule(new SearchModule())
                .detailsModule(new DetailsModule())
                .favoriteModule(new FavoriteModule())
                .compareModule(new CompareModule())
                .homeModule(new HomeModule())
                .categoriesModule(new CategoriesModule())
                .build() ;
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
