package com.example.mobology.root;

import com.example.mobology.Repository.RepositoryModule;
import com.example.mobology.categories.CategoriesFragment;
import com.example.mobology.categories.CategoriesModule;
import com.example.mobology.compare.CompareActivity;
import com.example.mobology.compare.CompareModule;
import com.example.mobology.details.DetailsModule;
import com.example.mobology.details.DeviceDetails;
import com.example.mobology.favorite.FavoriteFragment;
import com.example.mobology.favorite.FavoriteModule;
import com.example.mobology.home.HomeFragment;
import com.example.mobology.home.HomeModule;
import com.example.mobology.navigator.MainActivity;
import com.example.mobology.search.SearchModule;
import com.example.mobology.search.SearchView;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class ,
        SearchModule.class ,
        FavoriteModule.class ,
        DetailsModule.class ,
        CompareModule.class ,
        HomeModule.class ,
        CategoriesModule.class ,
        RepositoryModule.class})

public interface ApplicationComponent {

    void inject(MainActivity target) ;
    void inject(SearchView target) ;
    void inject(DeviceDetails target) ;
    void inject(FavoriteFragment target) ;
    void inject(CompareActivity target) ;
    void inject(HomeFragment target) ;
    void inject(CategoriesFragment target) ;
}
