package com.example.mobology.search;

import com.example.mobology.models.DeviceEntry;

import rx.Observable;

public interface SearchMVP {
    interface Presenter {
        void rxUnsubscribe();
        void requestSearchResults(String searchText) ;
        void setView(SearchMVP.View view) ;
    }
    interface Model {
        Observable<DeviceEntry> getResult(String searchText) ;
    }
    interface View {
        void showToastMessage(String message) ;
        void updateSearchResult(DeviceEntry searchResults) ;
    }
}
