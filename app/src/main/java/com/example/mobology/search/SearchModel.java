package com.example.mobology.search;

import com.example.mobology.Repository.Repository;
import com.example.mobology.models.DeviceEntry;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;


public class SearchModel implements SearchMVP.Model {
    Repository repository;

    public SearchModel(Repository searchRepository) {
        this.repository = searchRepository;
    }

    @Override
    public Observable<DeviceEntry> getResult(String searchText) {
        Observable<List<DeviceEntry>> searchResultObservable = repository.getSearchResults(searchText);
        return searchResultObservable.concatMap(
                new Func1<List<DeviceEntry>, Observable<DeviceEntry>>() {
                    @Override
                    public Observable<DeviceEntry> call(List<DeviceEntry> deviceEntries) {
                        if(deviceEntries == null || deviceEntries.size() == 0 )
                            return Observable.empty() ;
                        return Observable.from(deviceEntries);
                    }
                }
        );
    }
}