package com.example.mobology.search;

import com.example.mobology.Repository.Repository;
import com.example.mobology.Repository.RetrofitClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module()
public class SearchModule {

    @Provides
    public SearchMVP.Presenter provideSearchActivityPresenter(SearchMVP.Model searchModel){
        return  new SearchPresenter(searchModel) ;
    }
    @Provides
    public SearchMVP.Model provideSearchActivityModel(Repository repository){
        return new SearchModel(repository) ;
    }

}
