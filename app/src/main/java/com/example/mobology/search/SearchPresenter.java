package com.example.mobology.search;

import com.example.mobology.models.DeviceEntry;

import androidx.annotation.Nullable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SearchPresenter implements SearchMVP.Presenter {

    SearchMVP.Model model ;
    @Nullable
    SearchMVP.View view ;
    Subscription subscription = null ;

    public SearchPresenter(SearchMVP.Model model) {
        this.model = model;
    }

    @Override
    public void requestSearchResults(String searchText) {


        rxUnsubscribe();

        subscription = model.getResult(searchText)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DeviceEntry>() {
                    @Override
                    public void onCompleted() {
                        if(view != null){
                            view.showToastMessage("search Done");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null){
                            view.showToastMessage("Error getting Results ): ");
                        }
                    }

                    @Override
                    public void onNext(DeviceEntry deviceEntry) {
                        if(view != null){
                            view.updateSearchResult(deviceEntry);
                        }
                    }
                }) ;
    }

    @Override
    public void setView(SearchMVP.View view) {
        this.view = view ;
    }

    @Override
    public void rxUnsubscribe() {
        if(subscription != null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }
}
