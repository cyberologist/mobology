package com.example.mobology.search;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.R;
import com.example.mobology.models.ResultEntry;
import com.example.mobology.details.DeviceDetails;
import com.example.mobology.root.App;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.example.mobology.compare.CompareFragment.DEVICE_SELECT_RESULT_CODE;
import static com.example.mobology.compare.CompareFragment.SEARCHED_DEVICE_ID;
import static com.example.mobology.compare.CompareFragment.SEARCHED_DEVICE_NAME;
import static com.example.mobology.home.HomeFragment.AM_I_A_MAIN_SEARCH_ACTIVITY;
import static com.example.mobology.utils.consts.INTENT_DEVICE_ID;
import static com.example.mobology.utils.consts.INTENT_DEVICE_NAME;

public class SearchView extends AppCompatActivity implements SearchMVP.View  {

    private static final String TAG = "SearchView";

    @Inject
    SearchMVP.Presenter presenter ;

    RecyclerView recyclerView ;
    SearchAdapter searchAdabter ;
    android.widget.SearchView searchView ;

    List<ResultEntry> resultList ;
    Context context ;

    boolean isFromMainSearch ;
    boolean textIsEmpty = true ;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        ((App) getApplication()).getComponent().inject(this);

        context = this ;
        resultList = new ArrayList<>() ;

        Toolbar toolbar = findViewById(R.id.include) ;
        recyclerView = findViewById(R.id.search_suggestion_result) ;

        setSupportActionBar(toolbar);
        if( getSupportActionBar() != null ){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        isFromMainSearch = getIntent().getBooleanExtra(AM_I_A_MAIN_SEARCH_ACTIVITY , false ) ;
        Log.d(TAG, "onCreate: isFromMainSearch : " + isFromMainSearch );


        searchAdabter = new SearchAdapter() ;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(searchAdabter);
        recyclerView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager inputMethodManager =
                        (InputMethodManager) SearchView.this.getSystemService(
                                Activity.INPUT_METHOD_SERVICE);
                if( inputMethodManager != null &&  SearchView.this.getCurrentFocus() != null){
                    inputMethodManager.hideSoftInputFromWindow(
                            SearchView.this.getCurrentFocus().getWindowToken(), 0);
                }
                return false;
            }

        });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int callerId = item.getItemId() ;
        if( callerId == android.R.id.home){
            finish();
            return true ;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_menu , menu);

        final MenuItem searchItem = menu.findItem(R.id.app_bar_search) ;
        searchView = (android.widget.SearchView) searchItem.getActionView()  ;
        searchView.setIconified(false);
        searchView.setQueryHint("Whatever You Want");

        searchView.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(TAG, "onQueryTextSubmit: " + s);
                searchView.clearFocus();
                textIsEmpty = (s.length() == 0 );
                getSearchResOrSug(s) ;
                return  true ;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d(TAG, "onQueryTextChange: " + s );
                textIsEmpty = (s.length() == 0) ;
                getSearchResOrSug(s) ;
                return true ;
            }
        });
        searchView.setOnCloseListener(new android.widget.SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                finish() ;
                return true ;
            }
        });

        return true ;
    }
    public void getSearchResOrSug(String s ){
        resultList.clear();
        searchAdabter.notifyDataSetChanged();
        presenter.requestSearchResults(s) ;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this) ;
    }
    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this , message , Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateSearchResult(DeviceEntry searchResult) {

        ResultEntry resultEntry = new ResultEntry(
                searchResult.getName() ,
                searchResult.getBrand(),
                searchResult.getId() ,
                searchResult.getImageUrl()
        ) ;
        //TODO  what is the problem , may be cause different thread ?

        resultList.add(resultEntry);
//        searchAdabter.notifyItemInserted(resultList.size()-1);
        searchAdabter.notifyDataSetChanged();
        Log.d(TAG, "updateSearchResult : size : " + resultList.size() );
    }
    public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ResultViewHolder> {

        private static final String TAG = "SearchAdapter";

        @NonNull
        @Override
        public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent , false ) ;
            return new ResultViewHolder(v) ;
        }

        @Override
        public void onBindViewHolder(@NonNull ResultViewHolder holder, final int position) {

            if(textIsEmpty){
                holder.mainName.setText("         enter your search :)");
                holder.subName.setText("             see suggstion or press search icon");
                holder.resItem.setClickable(false);

            } else if (resultList.size() == 0 ){
                holder.mainName.setText("           Device not Found ):");
                holder.subName.setText("            try again , or search for another Device :)");
                holder.resItem.setClickable(false);
            } else {
                holder.mainName.setText(resultList.get(position).getMainName());
                holder.subName.setText(resultList.get(position).getSubName());
                holder.resItem.setClickable(true);
                holder.resItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(isFromMainSearch) {

                            Intent intent = new Intent(context, DeviceDetails.class);
                            intent.putExtra(INTENT_DEVICE_ID, resultList.get(position).getId());
                            intent.putExtra(INTENT_DEVICE_NAME, resultList.get(position).getMainName());
                            context.startActivity(intent);
                        } else {
                            Intent intent = new Intent() ;
                            intent.putExtra(SEARCHED_DEVICE_ID ,resultList.get(position).getId()) ;
                            intent.putExtra(SEARCHED_DEVICE_NAME,resultList.get(position).getMainName()) ;
                            ( (Activity) context ).setResult(DEVICE_SELECT_RESULT_CODE , intent) ;
                            ( (Activity) context ).finish() ;
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return resultList == null || resultList.size() == 0 ? 1 : resultList.size() ;
        }

        public class ResultViewHolder extends  RecyclerView.ViewHolder {
            TextView mainName ;
            TextView subName ;
            LinearLayout resItem ;
            public ResultViewHolder(View v){
                super(v) ;
                mainName = v.findViewById(R.id.search_main_name) ;
                subName = v.findViewById(R.id.search_sub_name) ;
                resItem = v.findViewById(R.id.search_item) ;

            }

        }
    }
}
