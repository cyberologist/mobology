package com.example.mobology.utils;

import com.example.mobology.models.DeviceEntry;
import com.example.mobology.models.ResultEntry;

import java.util.ArrayList;
import java.util.List;

public class randomUtills {
  /*
    public List<ResultEntry> fromEntryToResult(List<DeviceEntry> content ){


        if(content == null || content.size() == 0 )
            return null ;

        List<ResultEntry> newContent = new ArrayList<>(10);
        for( int i = 0 ; i < content.size() ; i ++ ){
            newContent.add(new ResultEntry(content.get(i).getName()
                    , content.get(i).getBrand()
                    , content.get(i).getId()
                    , content.get(i).getImageUrl())) ;
        }
        return newContent ;
    }
    */
//
//    public void setDevices(LiveData<List<DeviceEntry>> list) {
//        // this happen out of the main thread be default
//        list.observe(this, new Observer<List<DeviceEntry>>() {
//            @Override
//            // and this runs on the main thread by default
//            public void onChanged(List<DeviceEntry> deviceEntries) {
//                // this can access the ui component so there is no need for
//                // run any this on the runOnUiThrad ()
//                Log.d(TAG, "onChanged: retrieving data from database ");
//                mAdapter.refreshResultWith(fromEntryToResult(deviceEntries) ,deviceEntries);
//                for( DeviceEntry de : deviceEntries){
//                    Log.d(TAG, "setDevices: " + de.getId() );
//                }
//            }
//        });
//    }

    /*
        result.add(new Triplet<>(attrName.get(0) , firstdeviceEntry.getName()      , seconddeviceEntry.getName()) ) ;
        result.add(new Triplet<>(attrName.get(1) , firstdeviceEntry.getEdge()      , seconddeviceEntry.getEdge()) ) ;
        result.add(new Triplet<>(attrName.get(2) , firstdeviceEntry.getAnnounced() , seconddeviceEntry.getAnnounced()) ) ;
        result.add(new Triplet<>(attrName.get(3) , firstdeviceEntry.getStatus()    , seconddeviceEntry.getStatus() ) ) ;
        result.add(new Triplet<>(attrName.get(4) , firstdeviceEntry.getDimensions(), seconddeviceEntry.getDimensions() ) ) ;
        result.add(new Triplet<>(attrName.get(5) , firstdeviceEntry.getWeight()    , seconddeviceEntry.getWeight() ) ) ;
        result.add(new Triplet<>(attrName.get(6) , firstdeviceEntry.getSim()       , seconddeviceEntry.getSim() ) ) ;
        result.add(new Triplet<>(attrName.get(7) , firstdeviceEntry.getType()      , seconddeviceEntry.getType()) ) ;
        result.add(new Triplet<>(attrName.get(8) , firstdeviceEntry.getSize()      , seconddeviceEntry.getType()) ) ;
        result.add(new Triplet<>(attrName.get(9) , firstdeviceEntry.getResolution(), seconddeviceEntry.getResolution() ) ) ;
        result.add(new Triplet<>(attrName.get(10), firstdeviceEntry.getWlan()      , seconddeviceEntry.getWlan() ) ) ;
        result.add(new Triplet<>(attrName.get(11), firstdeviceEntry.getBluetooth() , seconddeviceEntry.getBluetooth() ) ) ;
        result.add(new Triplet<>(attrName.get(12), firstdeviceEntry.getGps()       , seconddeviceEntry.getGps() ) ) ;
        result.add(new Triplet<>(attrName.get(13), firstdeviceEntry.getUsp()       , seconddeviceEntry.getUsp() ) ) ;
        result.add(new Triplet<>(attrName.get(14), firstdeviceEntry.getBattery_c() , seconddeviceEntry.getBattery_c() ) ) ;
        result.add(new Triplet<>(attrName.get(15), firstdeviceEntry.getColors()    , seconddeviceEntry.getColors() ) ) ;
        result.add(new Triplet<>(attrName.get(16), firstdeviceEntry.getSensors()   , seconddeviceEntry.getSensors() ) ) ;
        result.add(new Triplet<>(attrName.get(17), firstdeviceEntry.getCpu()       , seconddeviceEntry.getCpu() ) ) ;
        result.add(new Triplet<>(attrName.get(18), firstdeviceEntry.getInternal()  , seconddeviceEntry.getInternal() ) ) ;
        result.add(new Triplet<>(attrName.get(19), firstdeviceEntry.getOs()        , seconddeviceEntry.getOs() ) ) ;
        result.add(new Triplet<>(attrName.get(20), firstdeviceEntry.getVideo()     , seconddeviceEntry.getVideo() ) ) ;
        result.add(new Triplet<>(attrName.get(21), firstdeviceEntry.getChipset()   , seconddeviceEntry.getChipset() ) ) ;
        result.add(new Triplet<>(attrName.get(22), firstdeviceEntry.getGpu()       , seconddeviceEntry.getGpu() ) ) ;
        result.add(new Triplet<>(attrName.get(23), firstdeviceEntry.getSingle()    , seconddeviceEntry.getSingle() ) ) ;
        result.add(new Triplet<>(attrName.get(24), firstdeviceEntry.getTriple()    , seconddeviceEntry.getTriple() ) ) ;
        result.add(new Triplet<>(attrName.get(25), firstdeviceEntry.getCharging()  , seconddeviceEntry.getCharging() ) ) ;

     */

    /*
        public class DevicesGetter {

        private Context context ;
        private Integer firstDeviceId  ;
        private Integer secondDeviceId  ;
        boolean respondedToTheFirst;
        boolean respondedToTheSecond;

        private DeviceEntry firstDeviceEntry ;
        private DeviceEntry secondDeviceEntry ;

        DevicesGetter(Context context ,Integer firstDeviceId  , Integer secondDeviceId){
            this.context = context ;
            this.secondDeviceId = secondDeviceId ;
            this.firstDeviceId = firstDeviceId ;
            respondedToTheFirst = false ;
            respondedToTheSecond = false ;
            firstDeviceEntry = null  ;
            secondDeviceEntry = null ;
        }

        void downloadDevices(){
//
//            Call<List<DeviceEntry>> call1 = MyRetrofit.getClient().getDeviceById(firstDeviceId);
//            Call<List<DeviceEntry>> call2 = MyRetrofit.getClient().getDeviceById(secondDeviceId);
//
//            call1.enqueue(new Callback<List<DeviceEntry>>() {
//                @Override
//                @EverythingIsNonNull
//                public void onResponse(Call<List<DeviceEntry>> call, Response<List<DeviceEntry>> response) {
//                    Log.d(TAG, "onResponse: goooood");
//                    respondedToTheFirst = true ;
//                    if(response.body() != null){
//                        firstDeviceEntry = response.body().get(0);
//                    }
//                    if(respondedToTheSecond){
//                        setAdapter();
//                    }
//                }
//
//                @Override
//                @EverythingIsNonNull
//                public void onFailure(Call<List<DeviceEntry>> call, Throwable t) {
//                    Log.d(TAG, "onFailure: baaad ");
//                    respondedToTheFirst = true ;
//                    t.printStackTrace();
//                    if(respondedToTheSecond){
//                        setAdapter();
//                    }
//
//                }
//            });
//            call2.enqueue(new Callback<List<DeviceEntry>>() {
//                @EverythingIsNonNull
//                @Override
//                public void onResponse(Call<List<DeviceEntry>> call, Response<List<DeviceEntry>> response) {
//                    Log.d(TAG, "onResponse: goooood");
//                    respondedToTheSecond = true ;
//                    if(response.body() != null){
//                        secondDeviceEntry = response.body().get(0);
//                    }
//                    if(respondedToTheFirst){
//                        setAdapter();
//                    }
//                }
//
//                @EverythingIsNonNull
//                @Override
//                public void onFailure(Call<List<DeviceEntry>> call, Throwable t) {
//                    Log.d(TAG, "onFailure: baaad ");
//                    t.printStackTrace();
//                    respondedToTheSecond = true ;
//                    if(respondedToTheFirst){
//                        setAdapter();
//                    }
//                }
//            });
        }

        void setAdapter(){

            ProgressBar compareLoading = findViewById(R.id.compare_loading_bar) ;
            compareLoading.setVisibility(View.GONE);

            LinearLayoutManager layoutManager = new LinearLayoutManager(context) ;

            CompareAdapter mAdapter = new CompareAdapter(getBaseContext() ,
                    classesToArray(firstDeviceEntry , secondDeviceEntry , context)) ;

            if( firstDeviceEntry != null && secondDeviceEntry != null ){
                compareDeviceImageSe.setVisibility(View.VISIBLE);
                compareDeviceImageSe.setImageResource(R.drawable.ic_launcher_foreground);
                compareDeviceImageFr.setVisibility(View.VISIBLE);
                compareDeviceImageFr.setImageResource(R.drawable.device_pic_plpaceholder);
                // TODO Change the actionbar title dynamically
            } else {
                Log.d(TAG, "setAdapter: x");
                compareDeviceImageSe.setVisibility(View.GONE);
                compareDeviceImageFr.setVisibility(View.VISIBLE);
                compareDeviceImageFr.setImageResource(R.drawable.device_pic_placeholder_error_loading);
                // TODO Change the actionbar title dynamically
            }

            RecyclerView compareDevicesAttrs ;
            compareDevicesAttrs = findViewById(R.id.compare_devices_attrs) ;

            compareDevicesAttrs.setAdapter(mAdapter);
            compareDevicesAttrs.setLayoutManager(layoutManager) ;

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(compareDevicesAttrs.getContext(),
                    layoutManager.getOrientation());

            compareDevicesAttrs.addItemDecoration(dividerItemDecoration);

        }
    }
     */

    /*



//            Call<List<DeviceEntry>> call = MyRetrofit.getClient().get10Random();
//            call.enqueue(new Callback<List<DeviceEntry>>() {
//                @EverythingIsNonNull
//                @Override
//                public void onResponse(Call<List<DeviceEntry>> call, Response<List<DeviceEntry>> response) {
//                    Log.d(TAG, "onResponse: body " + response.body() );
//                    populateTheUi(fromEntryToResult(response.body()));
//                    if( response.body() != null ){
//                        Log.d(TAG, "onResponse: gooood :) " + response.body().size());
//                    } else{
//                        Log.d(TAG, "onResponse: but baad  :( ");
//                    }
//                }
//
//                @EverythingIsNonNull
//                @Override
//                public void onFailure(Call<List<DeviceEntry>> call, Throwable t) {
//                    populateTheUi(null);
//                    Log.d(TAG, "onFailure: baaad ): ");
//                }
//            });
//



     */

    /*
    //            Call<List<DeviceEntry>> call  = MyRetrofit.getClient().getSearchResult(searchText) ;
//            call.enqueue(new Callback<List<DeviceEntry>>() {
//                @Override
//                public void onResponse(Call<List<DeviceEntry>> call, Response<List<DeviceEntry>> response) {
//                    Log.d(TAG, "onResponse: good :) ");
//                    staggeredRecyclerViewAdapter.refreshDataWith(response.body()) ;
//                }
//
//                @Override
//                public void onFailure(Call<List<DeviceEntry>> call, Throwable t) {
//                    Log.d(TAG, "onResponse: baad ): ");
//                    staggeredRecyclerViewAdapter.refreshDataWith(null) ;
//                }
//            });

     */

    //        result.add(new Pair<>(attrName.get(0) , deviceEntry.getBrand() ) ) ;
//        result.add(new Pair<>(attrName.get(1) , deviceEntry.getEdge() ) ) ;
//        result.add(new Pair<>(attrName.get(2) , deviceEntry.getAnnounced() ) ) ;
//        result.add(new Pair<>(attrName.get(3) , deviceEntry.getStatus() ) ) ;
//        result.add(new Pair<>(attrName.get(4) , deviceEntry.getDimensions() ) ) ;
//        result.add(new Pair<>(attrName.get(5) , deviceEntry.getWeight() ) ) ;
//        result.add(new Pair<>(attrName.get(6) , deviceEntry.getSim() ) ) ;
//        result.add(new Pair<>(attrName.get(7) , deviceEntry.getType() ) ) ;
//        result.add(new Pair<>(attrName.get(8) , deviceEntry.getSize() ) ) ;
//        result.add(new Pair<>(attrName.get(9) , deviceEntry.getResolution() ) ) ;
//        result.add(new Pair<>(attrName.get(10) , deviceEntry.getWlan() ) ) ;
//        result.add(new Pair<>(attrName.get(11) , deviceEntry.getBluetooth() ) ) ;
//        result.add(new Pair<>(attrName.get(12) , deviceEntry.getGps() ) ) ;
//        result.add(new Pair<>(attrName.get(13) , deviceEntry.getUsp() ) ) ;
//        result.add(new Pair<>(attrName.get(14) , deviceEntry.getBattery_c() ) ) ;
//        result.add(new Pair<>(attrName.get(15) , deviceEntry.getColors() ) ) ;
//        result.add(new Pair<>(attrName.get(16) , deviceEntry.getSensors() ) ) ;
//        result.add(new Pair<>(attrName.get(17) , deviceEntry.getCpu() ) ) ;
//        result.add(new Pair<>(attrName.get(18) , deviceEntry.getInternal() ) ) ;
//        result.add(new Pair<>(attrName.get(19) , deviceEntry.getOs() ) ) ;
//        result.add(new Pair<>(attrName.get(20) , deviceEntry.getVideo() ) ) ;
//        result.add(new Pair<>(attrName.get(21) , deviceEntry.getChipset() ) ) ;
//        result.add(new Pair<>(attrName.get(22) , deviceEntry.getGpu() ) ) ;
//        result.add(new Pair<>(attrName.get(23) , deviceEntry.getSingle() ) ) ;
//        result.add(new Pair<>(attrName.get(24) , deviceEntry.getTriple() ) ) ;
//        result.add(new Pair<>(attrName.get(25) , deviceEntry.getCharging() ) ) ;
 /*

        List<String> attrName = new ArrayList<>() ;

        InputStream is = context.getResources().openRawResource(R.raw.atrrrrs);
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        try{
            for (String line; (line = r.readLine()) != null ; ) {
                attrName.add(line) ;
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        Log.d(TAG, "deviceDetailsAdapter: attrsRaw size : " + result.size() );
        for( int i = 0 ; i < attrName.size() ; ++ i ){
            Log.d(TAG, "deviceDetailsAdapter: " + attrName.get(i));
        }

  */

 /*


    public static void addDeviceToDatabase(final Context context , final DeviceEntry deviceEntry){

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
        @Override
        public void run() {
            AppDatabase.getInstance(context).deviceDao().insertDevice(deviceEntry) ;
//         you cant do this
//        populateTheUi(nowCourseEntry);
//         you should do this to access ui element from a thread
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    populateTheUi(nowCourseEntry);
                        }
                   });
             }
        } );
    }


  @Override
    public void storeFragmentView(int fragmentId, View view) {
        switch (fragmentId){
            case consts.COMPARE_FRAGMENT :
                compareFragmentView = view ;
                break ;
            case consts.HOME_FRAGMENT :
                homeFragmentView = view ;
                break ;
                default:
                    // TODO what to do ?
        }
    }

    @Override
    public View restoreFragmentView(int fragmentId) {
        switch (fragmentId){
            case consts.COMPARE_FRAGMENT :
                if(compareFragmentView != null){
                    return compareFragmentView ;
                }
                break ;
            case consts.HOME_FRAGMENT :
                if(homeFragment != null){
                    return homeFragmentView ;
                }
                break ;
            default:
                // TODO what to do ?
        }
        return null ;
    }



  */

}
